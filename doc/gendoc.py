#!/usr/bin/env python


import os
import commands



current_dir = os.path.dirname(os.path.abspath(__file__))

origin_mobile= os.path.realpath(os.path.join(current_dir,"../droydserver/uiautomatorlibrary/Mobile.py"))
here_mobile= os.path.join(current_dir,"Mobile.py")

# copy original mobile file to here
with open (origin_mobile,"rb") as fin:
    with open(here_mobile,"wb") as fout:
        fout.write(fin.read())



# gen doc
source = os.path.join(current_dir,"../v-droyd/bin/activate")
gen_cmd = "python -m robot.libdoc Mobile.py Mobile.html"


r = commands.getstatusoutput("source %s && python -m robot.libdoc Mobile.py Mobile.html" % source)

# generate keywords for mobile operation
#python -m robot.libdoc Mobile.py Mobile.html


print r