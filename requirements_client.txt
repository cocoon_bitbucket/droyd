requests==2.7.0
robotframework==2.9.1
urllib3==1.12
wsgiref==0.1.2
docopt==0.6.2