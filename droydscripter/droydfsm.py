__author__ = 'cocoon'
"""

    finite state machines ( based on custom fysom ) to resolve uiautomator events


"""


import re
from fysom import Fysom, FysomError



class UiFsm(Fysom):
    """

        a Finite state Machine base on fysom for uiautomator events

    """

    # a set of triggers ( map lonf event names to short names)
    _triggers= {}


    def out(self,msg):
        """

        :param msg:
        :return:
        """
        print msg

    def set_out(self,out_function):
        """

        :param out_function:
        :return:
        """
        #set a out function: def out(self,msg)
        self.out=out_function



    def _trigger(self, event, *args, **kwargs):
        """
            low level trigger

        :param event: str , short event name
        :param args:
        :param kwargs:
        :return:
        """
        if not hasattr(self, event):
            raise FysomError(
                "There isn't any event registered as %s" % event)
        return getattr(self, event)(*args, **kwargs)

    def trigger(self, event_name, *args, **kwargs):
        '''
            Triggers the given event.
            The event can be triggered by calling the event handler directly, for ex: fsm.eat()
            but this method will come in handy if the event is determined dynamically and you have
            the event name to trigger as a string.
        '''
        return self._trigger(event_name,*args,**kwargs)


    def apply_interception_rule(self,event_line):
            """
                apply common rule from interception knowledge base

            :param context:
            :param rule:
            :param trace:
            :return:
            """
            context= event_line.context
            rule= event_line.rule


            lines=[]
            result=[]

            # extract action from rule
            try:
                action= rule['action']
            except KeyError:
                # no action defined in interceptions rules
                line= "# No action"
                self.out(line)
                return

            if action.has_key('cmd'):
                # generic cmd case
                result.append(action['cmd'])
                #result.append("${user%d}" % context['user'])
                result.append("${%s}" % context['user'])
                for key in action['selector']:
                    if key == 'textOrDescription':
                        if not context.get('ContentDescription',None) or context['ContentDescription']=="null":
                            # description is null take text
                            result.append('text=%s' % context['Text'].strip("[]"))
                        else:
                            # keep description
                            #result.append('content-desc=%s' % context['ContentDescription'])
                            result.append('description=%s' % context['ContentDescription'])
                    elif key == 'Text':
                        result.append('text=%s' % context['Text'].strip("[]"))
                    elif key == "ClassName":
                        result.append( "%s=%s" % ("className" , context[key]))
                    elif key == "PackageName":
                        result.append( "%s=%s" % ("packageName" , context[key]))
                    else:
                        prop=key[:]
                        prop[0]=key[0].lower()
                        result.append( "%s=%s" % (prop , context[key]))
                line= "  ".join(result)
            elif action.has_key('notification'):
                # notification case
                line= "# Notification ${%s}  %s" % (  context['user'], action['data'] )
            else:
                line= "# No Operation"
            self.out(line)
            #lines.append(line)
            #return lines




CALL_STATES= ['NULL','CALLING','INCOMING','EARLY','CONNECTING','CONFIRMED','DISCONNECTED']


# a fsm for calling application
class CallFsm(UiFsm):
    """
        a F S M  to handle
            script generation
                from uiautomator events
                for phone packages:  com.android.contacts, com.android.incallui




    """
    cfg= {
        'initial': 'NULL',
        'events': [
            {'name': 'call', 'src': '*', 'dst': 'CALLING'},
            {'name': 'hangup', 'src': ['CALLING','CONNECTING','CONFIRMED'], 'dst': 'DISCONNECTED'},
            {'name': 'answer', 'src': '*', 'dst': 'CONNECTING'},
            {'name': 'reject', 'src': '*', 'dst': 'NULL'},
            {'name': 'incoming_call', 'src': '*', 'dst': 'INCOMING'},
            {'name': 'notification_calltime', 'src':['CALLING','CONNECTING'], 'dst': 'CONFIRMED'} ,
            {'name': 'notification_calltime', 'src': '*', 'dst': '*'} ,
            {'name': 'notification_incallui', 'src': 'INCOMING', 'dst': 'CONFIRMED'} ,
            {'name': 'notification_incallui', 'src': '*', 'dst': '*'} ,
            {'name': 'reset', 'src': '*', 'dst': 'NULL'},
            #{'name': 'call_ended', 'src': ['CONFIRMED','DICONNECTED'], 'dst': 'DISCONNECTED'},
            {'name': 'wild', 'src': '*', 'dst': '*'},
        ]
    }


    _packageNames= [ 'com.android.contacts', 'com.android.incallui']

    _triggers= {

        'Press Dial Button': 'call',
        'Press Hangup Button': 'hangup',
        'Press Reject Button': 'reject',
        'Press Answer Button': 'answer',
        'Call Ended': 'call_ended',
        'Incoming Call': 'incoming_call',
        'notification incallui': 'notification_incallui',
        'notification call time':'notification_calltime',
        '*': 'wild'
     }

    calltime_pattern= re.compile(r'.*(\d+) minutes (\d+) seconds.*')


    def onchangestate(self,e):
        """

        :return:
        """
        # if hasattr(self,'event_line'):
        #     self.out("# Phone Fsm Change state for %s from %s to %s" % (e.event_line.context['user'],e.src, e.dst))
        self.out("# Phone Fsm Change state from %s to %s" % (e.src, e.dst))


    def onCALLING(self,e):
        """

        :return:
        """
        #print "enter calling"

        # generate the command with interceptions rules
        assert e.event_line
        self.apply_interception_rule(e.event_line)


        #self.out("    click   ${userA}    resourceId=com.android.contacts:id/dialButton")


    def onINCOMING(self,e):
        """

        :return:
        """
        self.out("Macro Phone wait incoming call  ${%s}" % e.event_line.context['user'] )



    def onbeforenotification_incallui(self,e):
        """
            receive event TYPE_NOTIFICATION_STATE_CHANGED ( contentView=com.android.incallui/0x7f040065)

            if current state is  "INCOMING"
                > generate the answer call OK
        :param e:
        :return:
        """
        #if self.current=="INCOMING":
        #    self.out("Macro Phone Answer Call     ${%s}" % e.event_line.context['user'])
        #return True

    def onbeforehangup(self,e):
        """

        :param e:
        :return:
        """
        self.out("Macro Phone hangup call     ${%s}"  % e.event_line.context['user'])

    def onreject(self,e):
        """

        :param e:
        :return:
        """
        self.out("# reject call")
        self.out("Click  ${%s}  className=android.widget.Button  text=Reject" %  e.event_line.context['user'])

    def onanswer(self,e):
        """

        :param e:
        :return:
        """
        self.out("# answer call")
        self.out("Click  ${%s}  className=android.widget.Button  text=Answer" %  e.event_line.context['user'])

    def onnotification_calltime(self,e):
        """

        :param e:
        :return:
        """
        content= e.event_line.context['ContentDescription']
        f= self.calltime_pattern.match(content)
        if f:
            minutes= f.group(1)
            seconds= f.group(2)
            #self.out("# call time (%s minutes %s seconds)" % (minutes,seconds))
            self.out("# call time %s (%s minutes %s seconds)" % (e.event_line.context['user'],minutes,seconds))
        else:
            self.out("# call time ???")


    def onCONFIRMED(self,e):
        """

        :return:
        """
        #print "call confirmed"


    def trigger(self, event_name, *args, **kwargs):
        '''
            Triggers the given event.
            The event can be triggered by calling the event handler directly, for ex: fsm.eat()
            but this method will come in handy if the event is determined dynamically and you have
            the event name to trigger as a string.

            eg trigger( "short_name"  , event=event )


        '''

        # check there is an EventLine
        event_line= kwargs['event']
        rule= event_line.rule

        # for debug
        if event_name == 'Incoming Call' or event_name == 'Press Reject Button':
            print event_name


        # check if rule is to be treated by this fsm
        if rule['context'].has_key('PackageName'):
            if not rule['context']['PackageName'] in self._packageNames:
                # this rule is not for us
                return -1

            # we take the rule
            # translate long event name to local event name
            event_name= rule['name']
            if event_name in self._triggers.keys():
                event= self._triggers[event_name]
            else:
                event='wild'


            r = self._trigger(event,event_line=event_line)
            return 0
        else:
            # no packagename in rule
            return -1



        # if kwargs['rule']['context'].has_key('PackageName'):
        #     if not kwargs['rule']['context']['PackageName'] in self._packageNames:
        #         # this rule is not for us
        #         return -1
        #
        #     # we take the rule
        #     # translate long event name to local event name
        #     event_name= kwargs['rule']['name']
        #     if event_name in self._triggers.keys():
        #         event= self._triggers[event_name]
        #     else:
        #         event=None
        #
        #     r = self._trigger(event,*args,**kwargs)
        #     return 0
        # else:
        #     # no packagename in rule
        #     return -1


class DefaultFsm(UiFsm):
    """
        a general purpose Fsm with only one state and one event type

        to treat uiautomator events  ( acts as a default treatment )

    """
    cfg= {
        'initial': 'null',
        'events': [
            {'name': 'wild', 'src': '*', 'dst': '*'},
        ]
    }

    def trigger(self, event_name, *args, **kwargs):
        '''
            Triggers the given event.
            The event can be triggered by calling the event handler directly, for ex: fsm.eat()
            but this method will come in handy if the event is determined dynamically and you have
            the event name to trigger as a string.
        '''
        event_line= kwargs['event']
        event="wild"
        r= self._trigger(event,event_line=event_line)
        # we have treated the event
        return 0


    def onwild(self,e):
        """

            on unique event type

        :param e:
        :return:
        """
        #print "generate event %s" % e

        # generate the command with interceptions rules
        self.apply_interception_rule(e.event_line)



class InspectFsm(UiFsm):
    """
        a Fsm to generate inspect script

        to treat uiautomator events and produce an inspect script

    """
    cfg= {
        'initial': 'null',
        'events': [
            {'name': 'wild', 'src': '*', 'dst': '*'},
            {'name': 'change_window', 'src': '*', 'dst': '*'},

        ]
    }

    _triggers= {
        'press Home': 'change_window',
        'Open Notification panel': 'change_window',
        'Incoming Call': 'change_window',
        'TYPE_WINDOW_STATE_CHANGED generic':'change_window',
        '*': 'wild'
     }



    def trigger(self, event_name, *args, **kwargs):
        '''
            Triggers the given event.
            The event can be triggered by calling the event handler directly, for ex: fsm.eat()
            but this method will come in handy if the event is determined dynamically and you have
            the event name to trigger as a string.
        '''

        if event_name in self._triggers.keys():
            # map general event name  name to local event name
            event= self._triggers[event_name]
        else:
            event='wild'


        event_line= kwargs['event']
        r= self._trigger(event,event_line=event_line)
        # we have treated the event
        return 0


    def onwild(self,e):
        """

            on unique event type

        :param e:
        :return:
        """
        #print "generate event %s" % e

        # generate the command with interceptions rules
        self.apply_interception_rule(e.event_line)

    def onchange_window(self,e):
        """



        :param e:
        :return:
        """
        #print "generate event %s" % e

        # generate the command with interceptions rules
        rule_name= e.event_line.rule['name']
        event_time=e.event_line.event_time
        event_time=event_time.replace(' ','_').replace(':','_').replace('.',('_'))
        user= e.event_line.context['user']
        snapshot_name= "%s-%s" %(user,event_time)
        if rule_name == 'TYPE_WINDOW_STATE_CHANGED generic':
            # generic case
            self.out("### window changed")
            self.out("Wait Update")
            self.out("snaphot  ${%s}  name=%s" % ( user,snapshot_name ))
        else:
            self.out("### window changed")
            self.apply_interception_rule(e.event_line)




if __name__== "__main__":


    fsm = CallFsm()



    fsm.call(arguments=dict(name="press dial buttton",description="dial call"))

    print fsm.current


    fsm.notification_incallui()
    print fsm.current

    fsm.notification_incallui()
    print fsm.current

    fsm.wild()



    fsm.call_ended()

    print fsm.current


    fsm.reset()
    print fsm.current


    fsm.incoming()

    print fsm.current

    fsm.notification_incallui()
    print fsm.current

    fsm.notification_incallui()
    print fsm.current



    fsm.hangup()
    print fsm.current


    fsm.reset()

    try:
        fsm.notification_incallui()
    except:
        pass
    print fsm.current


    print



