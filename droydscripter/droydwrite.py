__author__ = 'cocoon'
"""
    take several uiautomator_events_?.log file and make

    an ordered one with user property


"""


import glob

import heapq

import os
from droydcore import EventLine
from droydcore import EventFlow
from droydcore import Scripter


#from interceptions import Resolver

from interceptions import FsmResolver as Resolver

from interceptions import FsmInspector


#glob_base="samples/uiautomator_events_*.log"

#base_name= "samples/uiautomator_events_"

#r_indice= "samples/uiautomator_events_(\d+).log"


class EventFlowMulti(EventFlow):

    """


    """
    def __init__(self,base_name="uiautomator_events", title='macro generated script',devices=None,resolver=Resolver):
        """

        :param base_name:
        :param title:
        :return:
        """
        self.base_name= base_name
        self.glob_base= "%s_*.log" % base_name
        self.title= title
        self.devices = devices

        self.resolver= resolver.from_yaml()

    def clean_script(self):
        """

        :return:
        """
        files= glob.glob(self.glob_base)

        for file in files:
            os.unlink(file)


    def generate_script(self,filename=None):
        """

        :return:
        """
        code = "\n".join(self.generate_code())

        script = Scripter( code,title=self.title,devices=self.devices)
        script.out(filename)


    def generate_code(self):
        """

        :return:
        """
        lines=[]

        result=[]
        handlers=[]

        here= os.getcwd()

        files= glob.glob(self.glob_base)

        events= {}
        current= {}


        # list log files
        log_filenames=[]
        for file in files:
            fname=os.path.join(here,file)
            log_filenames.append(fname)

        # manage handleers list
        for fname in sorted(log_filenames):
            handlers.append(open(fname,'r'))

        for indice,handler in enumerate(handlers):

            events[indice]=[]
            current[indice]=0
            for line_number,line in enumerate(handler):
                event= EventLine(line)
                event.data['user']=indice +1
                event.data['source_line_number']=line_number

                if event.is_valid():
                    events[indice].append(event)
                    heapq.heappush(result,[event.event_time,event] )
                continue

        # generate header
        title= self.title
        documentation= "..."
        arguments=""
        #for i in range(len(handlers)):
        for i in range(len(self.devices)):
            arguments += "  ${user%d}" % (i+1)

        lines.extend(self.generate_header(title,documentation,arguments))


        # list in timestamp order
        for i in range(len(result)):
            priority , event = heapq.heappop(result)
            #heappop(h) for i in range(len(h))
            #print priority , event.data['user'], event
            user= "user%d" % event.data['user']

            #
            # generate the code line for an event
            #
            code_line= self.generate_line(event,user)

            #
            if code_line is not None:
                lines.extend(code_line)

        return lines




if __name__=="__main__":

    devices= ['aab','bbb']
    sample= "samples/A_CALL_B_NORESPONSE/droydscripter"
    sample= "samples/A_CALL_B_REJECTED/droydscripter"
    sample= "samples/A_CALL_B_OK/droydscripter"



    #devices= ['aab']

    def test_replay_generator():
        """

        :return:
        """


        g = EventFlowMulti(base_name=sample,title="macro test",devices=devices)

        g.generate_script()


    def test_inspect_generator():
        """

        :return:
        """


        g = EventFlowMulti(base_name=sample,title="macro test",devices=devices,resolver=FsmInspector)

        g.generate_script()

        return


    # begins
    #test_replay_generator()

    test_inspect_generator()



