droydnative
============


a standalone jenkins + adb + droydhub

start jenkins from this directory

```
fig up
```

find the port to open a browser

from a docker host session

```
docker ps

```


open jenkins in browser
connect an android device to your host

create job:

name uiautomator
type: freestyle

add a step:

shell
pybot /tests/demo.robot