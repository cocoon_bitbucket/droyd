__author__ = 'cocoon'
"""

    an gateway to droyd hub  ( native or http )


    usage:

        client = DroydHub( url = http_url )

        return a Native client if url is None

        else return an http client



"""
import inspect

NativeClient_error = None
HttpClient_error = None

try:
    # see if we can use HttpClient ( requests )
    from http import HttpDroydHub
except ImportError ,e:
    # no module requests
    HttpDroydHub = None
    HttpClient_error = e

try:
    # see if we can use native client ( uiautomator )
    from droydserver.native import NativeDroydHub
except ImportError ,e :
    # no module requests
    NativeDroydHub = None
    NativeClient_error = e



def DroydHub(url = None):
    """

        return a NativeClient or an HttpClient (if url not None )

    :param url:
    :return:
    """

    if not url:
        # select the native api to phone hub
        if not NativeDroydHub:
            raise ImportError('cannot load a NativeClient : %s' , str(NativeClient_error))
        # return a native client instance
        return NativeDroydHub()

    else:
        # select a Http client to phone hub
        if not HttpDroydHub:
            raise ImportError('cannot load an HttpClient: %s' % str(HttpClient_error))
        # return an HttpClient instance
        return HttpDroydHub(url)







class Gateway(object):
    """

        a high level api to either native hub or http hub


        NOT YET IMPLEMENTED


    """
    def __init__(self,url=None):
        """

        :param url:
        :return:
        """

        self._api= None
        self._native= True

        if not url:
            # select the native api to phone hub
            if not NativeDroydHub:
                raise ImportError('cannot load a NativeClient : %s' , str(NativeClient_error))
            # return a native client instance
            self._api= NativeDroydHub()

        else:
            # select a Http client to phone hub
            if not HttpDroydHub:
                raise ImportError('cannot load an HttpClient: %s' % str(HttpClient_error))
            # return an HttpClient instance
            self._api= HttpDroydHub(url)
            self._native=False



        # extract all methods of Mobile
        self._keywords = []
        for name,func in inspect.getmembers(self._api._mobile_factory, predicate=inspect.ismethod  ):
            if not name.startswith('_'):
                self._keywords.append(name)
        # add session keywords
        self._keywords.extend(['open_session','close_session','adb_devices','scan_devices'])

        #print "mobile keywords: %s" % self._keywords

    def get_keyword_names(self):
        """

        :return: the list of keywords
        """
        return self._keywords


    # dynamic access to api methods
    def __getattr__(self, name):
        if name not in ['excluded',]:
            return getattr(self._api,name)
        raise AttributeError("Non-existing attribute '%s'" % name)



    def method(self,name):
        """

        :param name: str the method name
        :return: return the method with corresponding name
        """
        m= getattr(self._api,name)
        return m



    def call(self, agent, obj, method, **selectors):
        """

        :param obj: a uidevice object if native , or a dictionary of selector elements if htttp
        :param method:
        :param selectors:
        :return:
        """
        if self._native:
            return self.method('call')
        else:
            raise NotImplementedError()



    def snapshot(self):
        """

        :return:
        """




if __name__== "__main__":



    print
