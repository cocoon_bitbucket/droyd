__author__ = 'cocoon'
"""
    robot_framework plugin for droyd hub


"""


import inspect
import os
import time, datetime


from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn

from gateway import DroydHub


# environment variable , None: local , else: the url of the Droyd Hub
RF_HUB_URL = None

#ROBOT_LIBRARY_SCOPE = "GLOBAL"
#ROBOT_LISTENER_API_VERSION = 3

# def start_suite(suite, result):
#     suite.tests.create(name='New test')
#
# def start_test(test, result):
#     test.keywords.create(name='Log', args=['Keyword added by listener!'])





class Pilot(object):
    """




    """

    # ROBOT_LISTENER_API_VERSION = 3
    # ROBOT_LIBRARY_SCOPE = "GLOBAL"
    #
    # def _end_suite(self, name, attrs):
    #     print 'Suite %s (%s) ending.' % (name, attrs['id'])


    def __init__(self,url=None,**kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        #self.ROBOT_LIBRARY_LISTENER = self


        #self._args = args
        self._kwargs = kwargs

        if not url:
            # check for environ 'RF_HUB_URL'
            RF_HUB_URL = os.environ.get('RF_HUB_URL',None)
        else:
            RF_HUB_URL= url

        self.RF_HUB_URL = RF_HUB_URL
        self._api = DroydHub(url= RF_HUB_URL)

        # extract all methods of Mobile
        self._keywords = []
        for name,func in inspect.getmembers(self._api._mobile_factory, predicate=inspect.ismethod  ):
            if not name.startswith('_'):
                self._keywords.append(name)
        # add session keywords
        self._keywords.extend(['open_session','close_session','adb_devices','scan_devices'])

        #print "mobile keywords: %s" % self._keywords

    def get_keyword_names(self):
        return self._keywords






    # dynamic access to api methods
    def __getattr__(self, name):
        if name not in ['excluded',]:
            return getattr(self._api,name)
        raise AttributeError("Non-existing attribute '%s'" % name)



    def screenshot(self,user,name=None):
        """

        :param user: str , the device id or user  (eg Alice ... )
        :param name: str , basename for the generated png file
        :return:
        """
        if not name:
            name= self.temporary_filename(user)
        name = name + ".png"
        img=self._api._screenshot(user)
        with open(name,'wb') as fh:
            data= img.decode("base64")
            fh.write(data)
        logger.info('\n<a href="%s"></a><br><img src="%s">' % (name, name), html=True)

        return name

    def dump(self,user,name=None):
        """

        :param selfself:
        :param user:
        :return:
        """
        if not name:
            name= self.temporary_filename(user)
        name= name + ".uix"
        dump= self._api._dump(user)
        with open(name,'w') as fh:
            fh.write(dump.encode('UTF-8'))
        return name

    def snapshot(self,user,name=None):
        """

        :param user: str
        :param name:
        :return:
        """
        name= self.temporary_filename(user,name)
        self.screenshot(user,name=name)
        self.dump(user,name=name)
        return name

    def timestamp(self):
        """
        output_dir = BuiltIn().get_variable_value('${OUTPUTDIR}')
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
        screenshot_path = '%s%s%s.png' % (output_dir, os.sep, st)
        """
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
        return st

    def temporary_filename(self,user,name=None):
        """
            return a filename user-timestamp

        :param user:
        :param name: str default name
        :return:
        """
        output_dir=''
        try:
            output_dir = BuiltIn().get_variable_value('${OUTPUTDIR}')
        except:
            pass
        if not name:
            name= "%s-%s" % (user, self.timestamp())
        return os.path.join(output_dir,name)


robot_plugin=Pilot


if __name__ == "__main__":


    p = Pilot()


    print