__author__ = 'cocoon'


import time
from droydclient.robot_plugin import Pilot
from droydclient.gateway import DroydHub


from scan_hierarchy import RilServiceMod


import subprocess

Alice=  "0a9b2e63"
Bob=    "95d03672"


ril_number= "*#0011#"

# selectors

quick_launch_phone= { 'className': 'android.widget.TextView', 'packageName':'com.sec.android.app.launcher', 'text': 'Phone'}

# long click on text zone
keypad_set_text= { 'resourceId':'com.android.contacts:id/digits' }

#  click delete button
keypad_deleteButton= { 'resourceId':'com.android.contacts:id/deleteButton' }


# set text with number
#keypad_set_text= { 'resourceId': 'com.android.contacts:id/digits' }

# press dial button
keypad_dialButton= { 'resourceId':'com.android.contacts:id/dialButton' }


contact_help= { 'resourceId': 'com.android.contacts:id/helpText'}





url= None
url= 'http://127.0.0.1:5000'



def test_select_by_index():
    """


    :return:
    """
    hub= Pilot()


    devices=hub.adb_devices()

    s = hub.open_session(Bob,)

    hub.press(Bob,key="home")


    index_phone_from_home="0/0/0/0/1/0/3/1/0/0"

    text= obj= hub.lt_get_info_by_index(Bob,index=index_phone_from_home, selector_name='text')
    action= hub.lt_click_by_index(Bob,index=index_phone_from_home, text=text)

    hub.press(Bob,key="home")
    action= hub.lt_click_by_index(Bob,index=index_phone_from_home)


    hub.press(Bob,key="home")
    action= hub.lt_long_click_by_index(Bob,index=index_phone_from_home)




def create_hub():

    hub= Pilot(url=url)

    devices=hub.adb_devices()

    serials= [ device[0] for device in devices]
    Alice,Bob = serials

    return hub, Alice, Bob


def create_hub_1():
    """


    :return:
    """
    hub= Pilot(url=url)

    devices=hub.adb_devices()

    serials= [ device[0] for device in devices]

    assert len(serials) == 1

    Alice  = serials[0]



    return hub, Alice



def current_test():
    """

    :return:
    """

    hub,Alice,Bob= create_hub()

    Dummy =  "95d99999"


    # test Alice outside a session
    try:
        hub.press(Alice,key="home")
        raise RuntimeError('should fail !')
    except RuntimeError,e:
        print e.message
    except KeyError,e:
        print e.message



    # basic test Alice Bob
    s = hub.open_session(Alice,Bob)

    check= hub.check_session(Alice,Bob)


    hub.press(Alice,key="home")
    hub.press(Bob,key="home")

    hub.close_session()


    # test Alice outside a session
    try:
        hub.press(Alice,key="home")
        raise RuntimeError('should fail !')
    except RuntimeError,e:
        print e.message
    except KeyError,e:
        print e.message



    # classic test Alice Dummy
    s= hub.open_session(Alice,Dummy)

    hub.press(Alice,key="home")

    # try:
    #     hub.press(Dummy,key="home")
    # except IOError:
    #     'RPC server not started!'
    #     pass

    hub.close_session()


    return


def get_text_test():

    hub,alice= create_hub_1()


    s = hub.open_session(alice)

    check= hub.check_session(alice)

    #hub.press_home(alice)

    hub.press(alice,key="home")


    #phone= hub.get_object(alice,**quick_launch_phone)

    #help = hub.get_object(alice,**contact_help)
    #info= hub.get_info_of_object(alice,help)

    #data= hub.native_operation(alice,operation='info', **contact_help)
    #print data

    hub.close_session()

    return




#adb_cmd = subprocess.Popen("which adb", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()





#hub.press_home(Alice)



#r= hub.execute_adb_shell_command (Alice,"dumpsys telephony.registry")
#r= hub.execute_adb_command(Alice,cmd="reboot")
#r= hub.execute_adb_command(Alice,cmd="wait-for-device")

# open phone app

# via obj dictionary
#hub.call(Alice, obj=quick_launch_phone ,  method= "click")

# # via object
# obj = hub.get_object(Alice, **quick_launch_phone)
# #hub.call(Alice,obj=obj,method='click')
# hub.click_on_object(Alice,object=obj)
#
# # the simplest
# #hub.click(Alice,**quick_launch_phone)
#
# #hub.wait_update(Alice)
# hub.sleep(Alice,1)
#
#
#
# #name= hub.snapshot(Alice,name='snapshot')
# name= hub.snapshot(Alice)
# #hub.screenshot(Alice)
# #hub.dump(Alice)
#
#
# # #hub.snapshot(Alice)
# # img= hub.low_level_screenshot(Alice)
# #
# # # write a png file with img
# # with open("my_img.png",'wb') as fh:
# #     data= img.decode("base64")
# #     fh.write(data)
# #
# # dump= hub.low_level_dump(Alice)
# # # write a uix file
# # with open("my_img.uix",'w') as fh:
# #     fh.write(dump.encode('UTF-8'))
#
#
# #
# hub.long_click(Alice,**keypad_set_text)
# hub.click(Alice,**keypad_deleteButton)
#
#
#
# hub.set_text(Alice,input_text=ril_number,**keypad_set_text)
#
#
# #hub.click(Alice,**keypad_dialButton)
#
# #xdump = hub.dump(Alice)
#
# #_data=RilServiceMod.from_xml_string(xdump).data
#
# #print data
#
# #img= hub.flash(Alice)
#
# #print img
#
#
#
# hub.press_home(Alice)
#
# hub.close_session()
#
#
# print


if __name__=="__main__":

    #current_test()

    get_text_test()
