__author__ = 'cocoon'
"""


    execute literal uiautomator commands like



"""

from uiautomator import Device




def native_operation( d, operation='info', modifiers='' ,**selectors):
    """


    :param selector: selector like d(className="android.widget.ListView
    :param modifier: literal like child_by_text("Wi-Fi",className="android.widget.LinearLayout")
    :param operation: click , long_click
    :return: return code of operation

    possible modifiers ( link modifiers with . )

    child(),child_by_text( text,className="" ),child_by_description(),child_by_instance()
    sibling()
    left(),right(className="android.widget.Switch")
    up(),down()

    """
    assert operation in [ 'exists' , 'info', 'count',
                          'click','click.bottomright', 'click.topleft',
                          'long_click','long_click.bottomright', 'long_click.topleft',
                          'clear_text',
                          ]
    uio = d(**selectors)
    if modifiers:
        cmd = 'uio.%s.%s' % ( modifiers, operation )
    else:
        cmd = 'uio.%s' % ( operation )
    if cmd not in ['exists', 'info','count']:
        cmd += '()'

    r= eval(cmd)
    return r



def _native_swipe( d, modifiers='' ,steps=10,**selectors):
    """


    :param selectors: selectors for the ui object like className="android.widget.ListView"
    :param modifier: literal like child_by_text("Wi-Fi",className="android.widget.LinearLayout")
    :param operation: click , long_click
    :return: return code of operation

    possible modifiers ( link modifiers with . )

    right(), left(), up(), down()

    """
    assert modifiers in [ 'right','left','up','down']
    operation='swipe'
    uio = d(**selectors)
    if modifiers:
        cmd = 'uio.%s.%s' % ( modifiers, operation )
    else:
        cmd = 'uio.%s' % ( operation )
    r= eval(cmd)
    return r

def swipe(self,steps=10,**selectors):
    """

    :param steps:
    :param seletors:
    :return:
    """
    return self.device(**selectors).swipe(steps=steps)

def swipe_left(self,steps=10,**selectors):
    """
        see swipe
    """
    return self.device(**selectors).swipe.left(steps=steps)

def swipe_right(self,steps=10,**selectors):
    """
        see swipe
    """
    return self.device(**selectors).swipe.right(steps=steps)

def swipe_up(self,steps=10,**selectors):
    """
        see swipe
    """
    return self.device(**selectors).swipe.up(steps=steps)

def swipe_down(self,steps=10,**selectors):
    """
        see swipe
    """
    return self.device(**selectors).swipe.down(steps=steps)


def drag_by_coordinates(self, sx, sy, ex, ey, steps=10):
    """
    Drag from (sx, sy) to (ex, ey) with steps

    See `Swipe By Coordinates` also.
    """
    self.device.drag(sx, sy, ex, ey, steps)


def drag_object_to_coordinates(self, x=0, y=0, steps=10, **selectors):
    """
    Drag object reached with  **selectors to (x, y) with steps

    See `Swipe By Coordinates` also.
    """
    return self.device(**selectors).drag.to(x=x, y=y ,steps=steps)

def drag_object_to_object(self, target , steps=10, **selectors):
    """
    Drag object reached with  **selectors to target

    target: specify

    See `Swipe By Coordinates` also.
    """

    uio= self.device(**selectors)
    if uio:
        cmd= "uio.drag.to( steps=%s, %s )" %  (str(steps),target)
        return eval(cmd)
    else:
        return None

def native_drag_to(self, target , steps=10, **selectors):
    """
    Drag object reached with  **selectors to target

    target: can be   x=1 ,y=2 or text="Clock"

    See `Swipe By Coordinates` also.
    """

    uio= self.device(**selectors)
    if uio:
        cmd= "uio.drag.to( steps=%s, %s )" %  (str(steps),target)
        return eval(cmd)
    else:
        return None




from droydclient.robot_plugin import Pilot



def create_hub_1():

    hub= Pilot()

    devices=hub.adb_devices()

    serials= [ device[0] for device in devices]
    Alice= serials[0]

    return hub, Alice



def create_hub_2():

    hub= Pilot()

    devices=hub.adb_devices()

    serials= [ device[0] for device in devices]
    Alice,Bob = serials

    return hub, Alice, Bob


def current_test():
    """

    :return:
    """


    #hub,Alice,Bob= create_hub_2()
    #s = hub.open_session(Alice,Bob)
    #hub.press(Alice,key="home")
    #hub.press(Bob,key="home")


    # hub,Alice= create_hub_1()
    # s = hub.open_session(Alice)
    # hub.press(Alice,key="home")
    # hub.close_session()


    Alice= "'0a9b2e63'"

    d = Device(Alice)

    d.press('Home')

    r= native_operation(d,'click','child().child(index=0)', resourceId= "com.sec.android.app.launcher:id/layout" )

    panel = d( resourceId= "com.sec.android.app.launcher:id/layout" )

    button= panel.child().child(index=0)
    assert button.info['text'] =='Phone'
    #button.click()


    # does not work
    button= panel.child_by_text('Phone')
    print button.info['text']

    button= panel.child_by_text('Phone',className="android.widget.TextView")
    print button.info['text']



    button = eval('panel.child_by_text("Phone",className="android.widget.TextView")'  )
    print button.info['text']

    button.click()

    return







if __name__=="__main__":

    current_test()
