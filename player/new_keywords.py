__author__ = 'cocoon'


import inspect

from uiautomator import AutomatorDevice, AutomatorDeviceObject


from droydserver.uiautomatorlibrary import Mobile



#original_methods= inspect.getmembers(AutomatorDevice, predicate=inspect.ismethod)
original_methods= inspect.getmembers(AutomatorDeviceObject, predicate=inspect.ismethod)

implemented_methods= inspect.getmembers(Mobile, predicate=inspect.ismethod)



print "original methods"
for e in original_methods:
    print e[0]

print "all impemented methods"
implemented_methods_names=[]
for e in implemented_methods:
    implemented_methods_names.append(e[0])
    print e[0]

print
print "original methods not in implemented"
for e in original_methods:
    name= e[0]
    if name not in implemented_methods_names:
        print name



print