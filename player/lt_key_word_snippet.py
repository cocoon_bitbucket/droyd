class Mobile():
    """

        from droydserver.uiautomatorlibrary.Mobile

    """


    # old work
    ## adds
    def _get_xml_hierarchy(self,source=None):
        """

        :param source: dump file or None
        :return: an instance of Elementree
        """
        if source:
            # get hierarchy from source file
            with open(source,'r') as fh:
                content= source.read()
                content=content.encode(encoding=("utf-8"))

        else:
            # get live hierarchy
            dmp=self.device.dump(compressed=False)
            content= dmp.encode(encoding='utf-8')
        # make element tree
        xml= ET.fromstring(content)
        return xml


    def _lt_next_index(self,element,index):
        """
            return the child with the given index in a aml node

        :param element: xml element
        :param index:
        :return:
        """
        for child in element:
            if child.attrib['index'] == index :
                # we found an element , return it
                return child
        #found nothing
        return None


    def _lt_get_index_path_node(self,element,index):
        """

        :param element: etree element
        :param index: string the path like "0/1/0"
        :return:
        """
        first=True
        for idx in index.split('/'):
            child= self._lt_next_index(element,idx)
            if child is not None:
                # found something
                first=False
                element=child
            else:
                # found nothing
                if first:
                    raise ValueError("more than one node with same index found: use different selectors")
                else:
                    raise ValueError("no such node, Maximum index value at current node is %d" % (len(element)-1))
        return element


    def _lt_element_and_coordinates_by_index_path(self, hierarchy,index_path):

        """
            look for an element given the index path

            return the selected xml element , x and y coordinates of an index path


        :param hierarchy: etree Element: root of a uiautomator xml dump
        :param index_path:
        :return:
        """
        element= self._lt_get_index_path_node(hierarchy,index_path)

        # getting bounds' coordinates and click
        if not element.attrib.has_key("bounds"):
            raise AttributeError("Selected element has no bounds determined by coordinates")
        bounds = re.findall("\[([0-9]+)\,([0-9]+)\]", element.attrib["bounds"])
        avg_x = (int(bounds[0][0]) + int(bounds[1][0]))/2
        avg_y = (int(bounds[0][1]) + int(bounds[1][1]))/2

        return element , avg_x , avg_y

    def _lt_action_by_index(self, index , function='click', **selectors):
        """

        :param index:
        :param selectors:
        :return:
        """
        xml= self._get_xml_hierarchy()

        element,x,y = self._lt_element_and_coordinates_by_index_path(xml,index)

        # check element with given selectors
        found=True
        for selector_name,selector_value in selectors.iteritems():
            xml_selector= self._selector_ui2xml(selector_name)

            if xml_selector in element.attrib:
                if not element.attrib[xml_selector] == selector_value:
                    found= False
                    break
            else:
                raise AttributeError("No such attribute %s in selected element" % selector_name)

        if found:
            if hasattr(self.device, function):
                try:
                    # call the device action (click, long click)
                    return getattr(self.device, function) (x, y)
                except Exception as e:
                    raise AttributeError("2 int() objects must be accepted by '%s' method"%function +
                                         "[original error:" + e.message + "]")
            else:
                raise AttributeError("'%s' method does not exist"%function)
        else:
            raise AttributeError("Element not found")


    def _selector_ui2xml(self,ui_selector_name):
        """
            convert a ui selector name to xml dump attribute name
                eg className -> class , resourceId -> resource-id

        :param ui_selector_name:
        :return:
        """
        convert= {
            'className': 'class', 'resourceId': 'resource-id'
        }
        try:
            return convert[ui_selector_name]
        except KeyError:
            # assume same name
            return ui_selector_name

    def lt_get_info_by_index (self, index, selector_name):
        """
        Returns the data contained by the attribute specified by 'selector'.
        """
        #dmp=self.device.dump(compressed=False)
        #buf= dmp.encode(encoding='utf-8')
        #xml= ET.fromstring(buf)
        xml= self._get_xml_hierarchy()

        element,x,y= self._lt_element_and_coordinates_by_index_path(xml,index)
        try:
            xml_selector=self._selector_ui2xml(selector_name)
            return element.attrib[xml_selector]
        except KeyError:
            raise AttributeError("No such attribute %s for the selected node." % selector_name)


    def lt_click_by_index (self, index, **selectors):
        """
        Makes a click on the given element specified by its index path
        """
        return self._lt_action_by_index(index=index, function='click', **selectors)


    def lt_long_click_by_index (self, index, **selectors):
        """
        Makes a long click on the given element specified by its index path
        """
        return self._lt_action_by_index(index=index,function='long_click',**selectors)

