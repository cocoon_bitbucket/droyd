
from uiautomator import device as d

mms_list_view = {'resourceId': "com.android.mms:id/history",
                'className': "android.widget.ListView"}

mms_text= {

    'resourceId':"com.android.mms:id/list_item_text_view",
    'className':"android.widget.TextView"
}

#d.press('HOME')


list_view= d(**mms_list_view)
print d.exists()
print d.info


d1= list_view.child( textContains='dernier')
if d1.exists:
    print d1.info
    print d1.text


d2= list_view.child(**mms_text)
if d2.exists:
    print d2.info
    print d2.text

d3= list_view.child(instance=1,**mms_text)
if d3.exists:
    print d3.info
    print d3.text



v= d(**mms_text)
if v.exists:
    if len(v):
        v= v[len(v)-1]
        print v.info
        print v.text





# get the child or grandchild
#d(className="android.widget.ListView").child(text="Bluetooth")



print "Done"