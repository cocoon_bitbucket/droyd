__author__ = 'cocoon'
"""

    setup a virtual env for test

    $ virtualenv venv
    $ . venv/bin/activate
    $ pip install --editable .



Release Notes

DroydRunner v0.2.14
==================

* fix RF 3.0 error message : [ ERROR ] Registering listeners .... 



DroydRunner v0.2.13
==================

* upgrade doc


DroydRunner v0.2.12
==================

* fix bug on get_child

DroydRunner v0.2.11
===================

* fix bug on adb devices
* add doctools ( doctools/interface_spec.py and doctools/Mobile.py)
* add short path to robot framework plugin : from droydclient import Droyd



DroydRunner v0.2.10
==================

* fix bug on remote apk install


DroydRunner v0.2.9
==================

* fix bug on scroll_to_vertically


DroydRunner v0.2.8
==================

* update from uiautomator wrapper



DroydRunner v0.2.7
==================

* documentation



DroydRunner v0.2.6
==================

keywords documentation update

removed keywords
================
* sleep
* set serial
* connect to wifi
* clear connected wifi

new keywords
-----------------
None

see mobile.html for details



"""


from setuptools import setup, find_packages


setup(
    name='droyd',
    version='0.2.14',
    license='',
    author='cocoon',
    author_email='tordjman.laurent@gmail.com',
    description='tools for android test automation',

    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'uiautomator',
        'robotframework',
        'Flask',
        'PyYAML',
        'docopt',
        'requests',

    ],
    # entry_points='''
    #     [console_scripts]
    #     yourscript=yourpackage.scripts.yourscript:cli
    # ''',
)