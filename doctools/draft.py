"""

msgid ""
msgstr ""
"Project-Id-Version: droydrunner\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"Language: fr\n"




"""




import ast
import os
import re

import logging
log= logging.getLogger('doctools')


header= '''\
msgid ""
msgstr ""
"Project-Id-Version: droydrunner\\n"
"POT-Creation-Date: \\n"
"PO-Revision-Date: \\n"
"Last-Translator: \\n"
"Language-Team: \\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=iso-8859-1\\n"
"Content-Transfer-Encoding: 8bit\\n"
"X-Generator: Poedit 1.8.7\\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\\n"
"Language: fr\\n"

'''

def signature_of_method(method):
    """
        return the sgnature of an ast Function definition

    :param method:
    :return:
    """
    assert isinstance(method,ast.FunctionDef)

    args = []
    if method.args.args:
        [args.append([a.col_offset, a.id]) for a in method.args.args]
    if method.args.defaults:
        pass
        #[args.append([a.col_offset, '=' + a.id]) for a in method.args.defaults]
        #[args.append([a.col_offset, '=' + a.s]) for a in method.args.defaults]
        for a in method.args.defaults:
            el_attr= a._fields[0]
            val= getattr(a,el_attr)
            if el_attr== 's':
                val= '"%s"' % val
            args.append([a.col_offset,"=%s" % val])
            continue

    sorted_args = sorted(args)
    for i, p in enumerate(sorted_args):
        if p[1].startswith('='):
            sorted_args[i-1][1] += p[1]
    sorted_args = [k[1] for k in sorted_args if not k[1].startswith('=')]

    if method.args.vararg:
        sorted_args.append('*' + method.args.vararg)
    if method.args.kwarg:
        sorted_args.append('**' + method.args.kwarg)

    signature = '(' + ', '.join(sorted_args) + ')'

    return signature

def extract_class_docstring(path,class_name=None):
    """

        extract method docstring from ast class

    :param class_:
    :return:
    """
    with open(path) as fd:
        content= fd.read()

    module= ast.parse(content)

    # find classes of module
    classes= [ node for node in module.body if isinstance(node,ast.ClassDef)]

    found = False
    for class_ in classes:
        if class_name:
            # we search a particuliar class_name
            if class_.name == class_name:
                # we found it return
                found =True
            else:
                # continue
                class_name= class_.name
                continue
        if not found:
            log.error("cannot find class: [%s] in file: [%s]" % (str(class_name),path))
            break

        # we found our class
        log.info("found class: [%s] in file: [%s]" % (str(class_name),path))


        # extract methods
        class_doc_string= ast.get_docstring(class_)
        # yield class doc_string
        yield class_name, "" , dict (docstring= class_doc_string)

        #methods= [ method for method in class_.body if isinstance(method,ast.FunctionDef)]

        for method in class_.body:
            if isinstance(method,ast.FunctionDef):
                # found a method
                data= dict( docstring= ast.get_docstring(method))


                data['signature'] = signature_of_method(method)
                yield class_name,method.name,data

        break

def get_msgid(class_name,method_name,comment=None):
    # comment= "# %s - %s" % (class_name,method_name)
    # return '%s\nmsgid "%s-%s\\n"' % (comment,class_name,method_name)
    return 'msgid "%s-%s\\n"' % (class_name,method_name)

def get_msgstr(data):

    lines= data.split('\n')

    output=[]
    output.append('msgstr ""')
    for line in lines:
        output.append('"%s\\n"' % line)

    return "\n".join(output)


def gen_po( path,class_name=None):
    """
        generate po file lines from ast

    """
    yield header
    for method in extract_class_docstring(path,class_name):
        class_name,method_name,data= method
        if method_name == "":
            # class_doc_string
            yield "# class: %s" % class_name
            method_name= "DOCSTRING"
        # generate msgid
        comment= "# %s - %s" % (class_name,method_name)
        yield comment
        if data.has_key('signature'):
            sig= "#, signature %s" % data['signature']
            yield sig
        yield get_msgid(class_name,method_name)
        yield get_msgstr(data['docstring'])


class_line_pattern= re.compile("# class: (\w+)")

msgid_line_pattern=  re.compile("msgid (.*)")
msgstr_line_pattern= re.compile("msgstr (.*)")

flag_line_pattern= re.compile("\#\, (\w+) (.*)")

def simple_read_po(path):
    """

        status: None , class , method , msgid , msgtr

    """
    with open(path,'r') as fd:
        lines= fd.readlines()

    status= None
    buffer= []
    for line in lines:
        #print line
        line=line.strip('\n')
        if status == None:
            # search for class line
            m = class_line_pattern.match(line)
            if m:
                status= "CLASS"
                yield 'CLASS' , m.group(1)
        elif status == "CLASS":
            # search for msgid line
            m = msgid_line_pattern.match(line)
            if m:
                # catch a msgid line
                status= 'MSGID'
                yield 'MSGID' , m.group(1)
            else:
                # not a msgid line search for flag
                m= flag_line_pattern.match(line)
                if m:
                    flag= m.group(1)
                    flag_value= m.group(2)
                    yield 'FLAG',"%s=%s" % ( flag,flag_value)
                    pass

        elif status == "MSGID":
            # search for a msgstr line
            m= msgstr_line_pattern.match(line)
            if m:
                status= "MSGSTR"
                buffer= []

        elif status == "MSGSTR":
            # search for comment
            if line.startswith('#'):
                # the end of the msgstr
                status= "CLASS"
                yield "MSGSTR", buffer
            else:
                buffer.append(line)
        else:
            raise ValueError('invalid status: %s' % status)
    # end of file
    if buffer:
        yield 'MSGSTR' , buffer

    return



def gen_interface(po_file):
    """
        generate a interface.py file from a po file

    """

    current_flags={}
    msgstr_indent = 8

    for entry in simple_read_po("./data/sample.po"):

        kind,data= entry

        if kind == 'CLASS':
            yield "class %s(object) :" % data
            #yield ''

        elif kind == 'FLAG':
            flag,content=data.split('=',1)
            current_flags[flag]=content
            pass

        elif kind== 'MSGID':

            data=data.strip('"')
            if data.endswith("\\n"):
                data=data[0:-2]
            class_name,method_name= data.split('-',1)

            if method_name == "DOCSTRING":
                msgstr_indent= 4
                pass
            else:
                if current_flags.has_key('signature'):
                    yield "    def %s%s:" %( method_name,current_flags['signature'])
                    current_flags={}
                else:
                    yield "    def %s(self,*args,**kwargs):" % method_name

        elif kind == 'MSGSTR':

            spaces= " " * msgstr_indent

            # reset DOCSTRING indent
            msgstr_indent= 8


            yield spaces + '"""'
            for line in data:
                line=line.strip('"')
                if line.endswith("\\n"):
                    line=line[0:-2]
                yield spaces + line   # "         %s" % line
                #yield "        %s" % line.strip('"')
            yield spaces + '"""'
            yield ''


if __name__=="__main__":


    #logging.basicConfig(level='DEBUG')

    spec_file= "./data/interface_spec.py"


    #print "BEGIN"


    # for line in extract_class_docstring(spec_file,'Mobile'):
    #     #log.debug(line)
    #     print line


    # for line in gen_po(spec_file,'Mobile'):
    #     print line

    # for line in simple_read_po("./data/sample.po"):
    #     print line


    for line in gen_interface("./data/sample.po"):
        print line




    #print "DONE."