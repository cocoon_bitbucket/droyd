class Mobile(object) :
    """
    DroydRunner keywords documentation


    The keywords marked "untested by ATAQ" are not tested for each release. They
    might work but are not guaranteed. Despite this you can try to use them. If
    you find a non working "untested" keyword, you can contact the support and
    we will try to make it tested and working for the next releases.
    """

    def open_session(self, *device_ids):
        """
            open a phone session:
                store accounts
        :param accounts:
        :return:
        """

    def check_session(self, *device_ids):
        """
        :param device_ids:
        :return:
        """

    def close_session(self):
        """
            close current session

        :param session_id:
        :return:
        """

    def adb_devices(self):
        """
        :return: list of android connected devices
        """

    def scan_devices(self):
        """
        list the connected devices and return info (list)

           :return:
               - nb (int) number of connected devices
               - device_names (list)  a list of connected device names
               - device_status (dict) the status of devices
               - tags  a list of names ( userA , userB ... to set tags )
        """

    def install(self, device_id, apk_path="./apk/OrangeDemoApp.apk"):
        """
            upload apk to server ,

        :param device_id:
        :param apk_path:
        :return:
        """

    def wait_update(self):
        """

        """

    def wait_idle(self):
        """

        """

    def dump(self, filepath=None):
        """
        get the dumped window hierarchy content(unicode)
        """

    def flash(self, scale=1.0, quality=100):
        """
            return base64 screenshot

        :return:
        """

    def get_object_data(self, **selectors):
        """
            DEPRECATED use get_object instead

        Get data dictionary of the UI object with selectors *selectors*

        See `introduction` for details about identified UI object.

        Example:
        | ${main_layer} | Get object_data | className=android.widget.FrameLayout | index=0 | # Get main layer which class name is FrameLayout |
        """

    def scroll_to_vertical(self, scroll_class, **selectors):
        """
        Scroll(vertically) on the object found with classname=scroll_class: obj to specific UI object which has *selectors* attributes appears.

        Return

        Example:

        | ${obj_data}    | Get Object Data      | className==android.widget.ListView |              | # Get the list object     |
        | ${found} | Scroll to Vertical | scroll_class=${obj_data['className']} | text=WebView | # Scroll to text:WebView. |
        """

    def scroll_to_horizontal(self, scroll_class, **selectors):
        """
        Scroll(horizontally) on the object found with classname=scroll_class: obj to specific UI object which has *selectors* attributes appears.

        Return data of the target obj

        Example:

        | ${obj_data}    | Get Object Data      | className==android.widget.ListView |              | # Get the list object     |
        | ${target_data} | Scroll to Horizontal | scroll_class=${obj_data['className']} | text=WebView | # Scroll to text:WebView. |
        """

    def click_tab_by_text(self, className, text):
        """
        select all objects with className  ( eg android.app.ActionBar$Tab )
        for each find a child with text= text
        if found : click on it
        """

    def click_linear_by_text(self, className, text):
        """
        select all objects with className  ( eg android.app.ActionBar$Tab )
        for each find a child with text= text
        if found : click on it
        """

    def snapshot(self, name=None, scale=1.0, quality=100):
        """
        take a screenshot  and save to  snapshot_<name>.png
        take a dump and save to snapshot_<name>.uix

        if no name specified take a random uuid
        """

    def get_device_info(self):
        """
        Retrieve the device info.

        The keyword will return a dictionary.

        You can log the information by using the log dictionary keyword in build in Collections library(http://robotframework.googlecode.com/hg/doc/libraries/Collections.html?r=2.8.4).

        Example:
        | ${device_info} | Get Device Info |
        | Log Dictionary | ${device_info}  |

        =>

        Dictionary size is 9 and it contains following items:

        currentPackageName: com.android.keyguard

        displayHeight: 1776

        displayRotation: 0

        displaySizeDpX: 360

        displaySizeDpY: 640

        displayWidth: 1080

        naturalOrientation: True

        productName: hammerhead

        sdkInt: 19


        Or get specific information of the device by giving the key.

        | ${device_info}  | Get Device Info |   |                |
        | ${product_name} | Get From Dictionary | ${device_info} | productName |

        =>

        ${product_name} = hammerhead
        """

    def turn_on_screen(self):
        """
        Turn on the screen.
        """

    def turn_off_screen(self):
        """
        Turn off the screen.
        """

    def press(self, key):
        """
        press a key

        key can be in [ home,back,right,left,up,down,center,menu,search,enter,delete,volume_up,volume_down,camera,power]
        or a keycode

        You can find all keycode in http://developer.android.com/reference/android/view/KeyEvent.html

        :param name: str name in home,back,left,right,...
        :return:
        """

    def press_home(self):
        """
        Press home key.
        """

    def click_at_coordinates(self, x, y):
        """
        Click at (x,y) coordinates.
        """

    def swipe_by_coordinates(self, sx, sy, ex, ey, steps=10):
        """
        Swipe from (sx, sy) to (ex, ey) with *steps* .

        Example:
        | Swipe By Coordinates | 540 | 1340 | 940 | 1340 |     | # Swipe from (540, 1340) to (940, 100) with default steps 10 |
        | Swipe By Coordinates | 540 | 1340 | 940 | 1340 | 100 | # Swipe from (540, 1340) to (940, 100) with steps 100        |
        """

    def drag_by_coordinates(self, sx, sy, ex, ey, steps=10):
        """
        ** Untested by ATAQ **

        Drag from (sx, sy) to (ex, ey) with steps

        See `Swipe By Coordinates` also.
        """

    def wait_for_exists(self, timeout=0, **selectors):
        """
        ** Untested by ATAQ **

        Wait for the object which has *selectors* within the given timeout.

        Return true if the object *appear* in the given timeout. Else return false.
        """

    def wait_until_gone(self, timeout=0, **selectors):
        """
        ** Untested by ATAQ **

        Wait for the object which has *selectors* within the given timeout.

        Return true if the object *disappear* in the given timeout. Else return false.
        """

    def wait_for_object_exists(self, obj, timeout=0):
        """
        Wait for the object: obj within the given timeout.

        Return true if the object *appear* in the given timeout. Else return false.
        """

    def wait_until_object_gone(self, obj, timeout=0):
        """
        Wait for the object: obj within the given timeout.

        Return true if the object *disappear* in the given timeout. Else return false.
        """

    def fling_forward_horizontally(self, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling forward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """

    def fling_backward_horizontally(self, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling backward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """

    def fling_forward_vertically(self, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling forward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """

    def fling_backward_vertically(self, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling backward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """

    def scroll_to_beginning_horizontally(self, steps=10, **selectors):
        """
        Scroll the object which has *selectors* attributes to *beginning* horizontally.

        See `Scroll Forward Vertically` for more details.
        """

    def scroll_to_end_horizontally(self, steps=10, **selectors):
        """
        Scroll the object which has *selectors* attributes to *end* horizontally.

        See `Scroll Forward Vertically` for more details.
        """

    def scroll_forward_horizontally(self, steps=10, **selectors):
        """
        Perform scroll forward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """

    def scroll_backward_horizontally(self, steps=10, **selectors):
        """
        Perform scroll backward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """

    def scroll_to_horizontally(self, obj, **selectors):
        """
        Scroll(horizontally) on the object: obj to specific UI object which has *selectors* attributes appears.

        Return true if the UI object, else return false.

        See `Scroll To Vertically` for more details.
        """

    def scroll_to_beginning_vertically(self, steps=10, **selectors):
        """
        Scroll the object which has *selectors* attributes to *beginning* vertically.

        See `Scroll Forward Vertically` for more details.
        """

    def scroll_to_end_vertically(self, steps=10, **selectors):
        """
        Scroll the object which has *selectors* attributes to *end* vertically.

        See `Scroll Forward Vertically` for more details.
        """

    def scroll_forward_vertically(self, steps=10, **selectors):
        """
        Perform scroll forward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        Example:
        | ${can_be_scroll} | Scroll Forward Vertically | className=android.widget.ListView       |                                   | # Scroll forward the UI object with class name |
        | ${can_be_scroll} | Scroll Forward Vertically | 100                                     | className=android.widget.ListView | # Scroll with steps |
        """

    def scroll_backward_vertically(self, steps=10, **selectors):
        """
        Perform scroll backward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """

    def scroll_to_vertically(self, obj, **selectors):
        """
        Scroll(vertically) on the object: obj to specific UI object which has *selectors* attributes appears.

        Return true if the UI object, else return false.

        Example:

        | ${list}        | Get Object           | className=android.widget.ListView |              | # Get the list object     |
        | ${is_web_view} | Scroll To Vertically | ${list}                           | text=WebView | # Scroll to text:WebView. |
        """

    def get_screen_orientation(self):
        """
        ** Untested by ATAQ **

        Get the screen orientation.

        Possible result: natural, left, right, upsidedown

        See for more details: https://github.com/xiaocong/uiautomator#screen-actions-of-the-device
        """

    def set_screen_orientation(self, orientation):
        """
        ** Untested by ATAQ **

        Set the screen orientation.

        Input *orientation* : natural or n, left or l, right or r, upsidedown (support android version above 4.3)

        The keyword will unfreeze the screen rotation first.

        See for more details: https://github.com/xiaocong/uiautomator#screen-actions-of-the-device

        Example:

        | Set Screen Orientation | n       | # Set orientation to natural |
        | Set Screen Orientation | natural | # Do the same thing          |
        """

    def freeze_screen_rotation(self):
        """
        ** Untested by ATAQ **

        Freeze the screen auto rotation
        """

    def unfreeze_screen_rotation(self):
        """
        ** Untested by ATAQ **

        Un-Freeze the screen auto rotation
        """

    def register_click_watcher(self, watcher_name, selectors, *condition_list):
        """
        ** Untested by ATAQ **

        The watcher click on the object which has the *selectors* when conditions match.
        """

    def register_press_watcher(self, watcher_name, press_keys, *condition_list):
        """
        ** Untested by ATAQ **

        The watcher perform *press_keys* action sequentially when conditions match.
        """

    def remove_watchers(self, watcher_name=None):
        """
        ** Untested by ATAQ **

        Remove watcher with *watcher_name* or remove all watchers.
        """

    def list_all_watchers(self):
        """
        ** Untested by ATAQ **

        Return the watcher list.
        """

    def get_object(self, **selectors):
        """
        Get the UI object with selectors *selectors*

        See `introduction` for details about identified UI object.

        Example:
        | ${main_layer} | Get Object | className=android.widget.FrameLayout | index=0 | # Get main layer which class name is FrameLayout |
        """

    def get_child(self, obj, **selectors):
        """
        ** Untested by ATAQ **

        Get the child or grandchild UI object from the *object* with *selectors*
        Example:
        | ${root_layout}   | Get Object | className=android.widget.FrameLayout |
        | ${child_layout}  | Get Child  | ${root_layout}                       | className=LinearLayout |
        """

    def get_sibling(self, obj, **selectors):
        """
        ** Untested by ATAQ **

        Get the sibling or child of sibling UI object from the *object* with *selectors*
        Example:
        | ${root_layout}     | Get Object   | className=android.widget.FrameLayout |
        | ${sibling_layout}  | Get Sibling  | ${root_layout}                       | className=LinearLayout |
        """

    def get_count(self, **selectors):
        """
        ** Untested by ATAQ **

        Return the count of UI object with *selectors*

        Example:

        | ${count}              | Get Count           | text=Accessibility    | # Get the count of UI object text=Accessibility |
        | ${accessibility_text} | Get Object          | text=Accessibility    | # These two keywords combination                |
        | ${count}              | Get Count Of Object | ${accessibility_text} | # do the same thing.                            |
        """

    def get_count_of_object(self, obj):
        """
        ** Untested by ATAQ **

        Return the count of given UI object

        See `Get Count` for more details.
        """

    def get_info_of_object(self, obj, selector=None):
        """
        return info dictionary of the *obj*
        The info example:
        {
         u'contentDescription': u'',
         u'checked': False,
         u'scrollable': True,
         u'text': u'',
         u'packageName': u'com.android.launcher',
         u'selected': False,
         u'enabled': True,
         u'bounds':
                   {
                    u'top': 231,
                    u'left': 0,
                    u'right': 1080,
                    u'bottom': 1776
                   },
         u'className': u'android.view.View',
         u'focusable': False,
         u'focused': False,
         u'clickable': False,
         u'checkable': False,
         u'chileCount': 1,
         u'longClickable': False,
         u'visibleBounds':
                          {
                           u'top': 231,
                           u'left': 0,
                           u'right': 1080,
                           u'bottom': 1776
                          }
        }
        """

    def click(self, **selectors):
        """
        Click on the UI object with *selectors*

        | Click | text=Accessibility | className=android.widget.Button | # Click the object with class name and text |
        """

    def click_on_object(self, object):
        """
        ** Untested by ATAQ **

        Click on the UI object which is gained by `Get Object`.

        Example:
        | ${button_ok}    | text=OK      | className=android.widget.Button |
        | Click on Object | ${button_ok} |
        """

    def long_click(self, **selectors):
        """
        Long click on the UI object with *selectors*

        See `Click` for more details.
        """

    def ui_object(self, obj):
        """
        ** Untested by ATAQ **


            return a ui object from a dictionary of selector properties

        :param obj: instance of AutomatorDeviceObject , or dict ( selector properties )
        :return: return a local device ui object
        """

    def flatten_ui_objet(self, obj):
        """
        ** Untested by ATAQ **

            serialize a ui object to a dict of selector properties

        :param obj: instance of  AutomatorDeviceObject
        :return: dict : dict of selector properties
        """

    def call(self, obj, method, **selectors):
        """
        ** Untested by ATAQ **

        This keyword can use object method from original python uiautomator

        See more details from https://github.com/xiaocong/uiautomator

        Example:

        | ${accessibility_text} | Get Object            | text=Accessibility | # Get the UI object                        |
        | Call                  | ${accessibility_text} | click              | # Call the method of the UI object 'click' |
        """

    def set_text(self, input_text, **selectors):
        """
        ** Untested by ATAQ **

        Set *input_text* to the UI object with *selectors*
        """

    def set_object_text(self, input_text, obj):
        """
        ** Untested by ATAQ **

        Set *input_text* the *object* which could be selected by *Get Object* or *Get Child*
        """

    def clear_text(self, **selectors):
        """
        ** Untested by ATAQ **

        Clear text of the UI object  with *selectors*
        """

    def open_notification(self):
        """
        Open notification

        Built in support for Android 4.3 (API level 18)

        Using swipe action as a workaround for API level lower than 18
        """

    def open_quick_settings(self):
        """
        Open quick settings

        Work for Android 4.3 above (API level 18)
        """

    def _sleep(self, time):
        """
        Sleep(no action) for *time* (in millisecond)
        """

    def uninstall(self, package_name):
        """
        ** Untested by ATAQ **

        Uninstall the APP with *package_name*
        """

    def execute_adb_command(self, cmd):
        """
        Execute adb *cmd*
        """

    def execute_adb_shell_command(self, cmd):
        """
        Execute adb shell *cmd*
        """

    def type(self, input_text):
        """
        ** Untested by ATAQ **

        [IME]

        Type *text* at current focused UI object
        """

    def start_test_agent(self):
        """
        ** Untested by ATAQ **

        [Test Agent]

        Start Test Agent Service
        """

    def stop_test_agent(self):
        """
        ** Untested by ATAQ **

        [Test Agent]

        Stop Test Agent Service
        """

    def get_info_by_index(self, index, selector, dump_file=None):
        """
        Return the data contained by the attribute specified by 'selector'.
        :param selector: the selector type of the element you want to read from.
        selectors can be any of the element's attributes.
        :param index: Index path. Separated by slashes.
        :param dump_file: XML file on which to operate, instead of a XML screen dump file.

        :return unicode: unicode() object containing the selector value.

        :raise AttributeError: if the specified selector is not found in the element
        specified by the index path.
        :raise ValueError: If no node found, or if more than one node with
        the same index path has been found (i.e. the XML dump is not well-formed),
        or if the selected attribute does not have the same value as expected.
        """

    def click_by_index(self, index, dump_file=None, **selectors):
        """
        Makes a click on the given element specified by its index path.
        The following arguments are required:

        :param index: Index path. Separated by slashes.
        :param <selector>: any of the elements attributes,
        including but not limited to: className, resourceId, bounds, text, &c.

        :raise AttributeError: view error messages for details.
        :raise ValueError: If no node found, or if more than one node with
        the same index path has been found (i.e. the XML dump is not well-formed),
        or if the selected attribute does not have the same value as expected.
        """

    def long_click_by_index(self, index, dump_file=None, **selectors):
        """
        Makes a click on the given element specified by its index path.
        :param index: Index path. Separated by slashes.
        :param <selector>: any of the elements attributes,
        including but not limited to: className, resourceId, bounds, text, &c.

        :raise AttributeError: view error messages for details.
        :raise ValueError: If no node found, or if more than one node with
        the same index path has been found (i.e. the XML dump is not well-formed),
        or if the selected attribute does not have the same value as expected.
        """

    def native_operation(self, operation="info", modifiers="", **selectors):
        """
        :param selector: selector like d(className="android.widget.ListView
        :param modifier: literal like child_by_text("Wi-Fi",className="android.widget.LinearLayout")
        :param operation: click , long_click
        :return: return code of operation

        possible modifiers ( link modifiers with . )

        child(),child_by_text( text,className="" ),child_by_description(),child_by_instance()
        sibling()
        left(),right(className="android.widget.Switch")
        up(),down()


        operation must one of exists,info,count,click,click.bottomright,click.topleft,
            long_click,long_click.bottomright,long_click.topleft,clear_text
        """

    def native_drag_to(self, target="", steps=10, **selectors):
        """
        Drag object reached with  **selectors to target

        target: can be   x=1 ,y=2 or text="Clock"
        """

    def native_fling(self, modifiers="", max_swipes=1000, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling on the specific scrollable ui object reachable via **selectors

        Possible modifiers:

            forward (default) , verticaly (default)
            backward          , horizentally
            toBeginning       ,  toEnd

            horiz.toBeginning , horiz.toEnd , horiz.backward , horiz.forward
            vert.toBeginning , vert.toEnd , vert.backward , vert.forward
        """

    def native_scroll(self, modifiers="", steps=100, max_swipes=1000, **selectors):
        """
        Perform scroll on the specific ui object(scrollable) reachable via **selectors

        Possible properties:

        horiz or vert
        forward or backward or toBeginning or toEnd, or to(text=..)


        :return:
        """

    def native_gesture(self, gesture="", **selectors):
        """
        ** Untested by ATAQ **

        Two point gesture from one point to another

            d(text="Settings").gesture((sx1, sy1), (sx2, sy2)).to((ex1, ey1), (ex2, ey2))

            gesture=  gesture((sx1, sy1), (sx2, sy2)).to((ex1, ey1), (ex2, ey2))


        :return:
        """

    def swipe_left(self, steps=10, **selectors):
        """
        see swipe
        """

    def swipe_right(self, steps=10, **selectors):
        """
        see swipe
        """

    def swipe_up(self, steps=10, **selectors):
        """
        see swipe
        """

    def swipe_down(self, steps=10, **selectors):
        """
        see swipe
        """

    def pinch_in(self, percent=100, steps=10, **selectors):
        """
        ** Untested by ATAQ **

            Two point gesture on the specific ui object reachable via **selectors

            In, from edge to center
        """

    def pinch_out(self, percent=100, steps=10, **selectors):
        """
        ** Untested by ATAQ **

           Two point gesture on the specific ui object reachable via **selectors

            Out, from center to edge
        """

