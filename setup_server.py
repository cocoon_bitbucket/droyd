from distutils.core import setup

setup(
    name='droyd',
    version='0.2.13',
    packages=[ 'droydclient',
               'droydserver','droydserver.lib','droydserver.lib.facets','droydserver.utils','droydserver.server',
               'droydserver.uiautomatorlibrary'],
    url='',
    license='',
    author='cocoon',
    author_email='tordjman.laurent@gmail.com',
    description='tools for android test automation'
)
