# -*- coding: utf-8 -*-
from robot.api import logger
from uiautomator import Device
from uiautomator import AutomatorDeviceObject
import subprocess
import os
import re
import time
import datetime
from robot.libraries.BuiltIn import BuiltIn
import xml.etree.ElementTree as ET
"""


    DroydRunner Mobile keywords




"""

adb_locations = ['/usr/local/bin/adb', '/usr/bin/adb']


class TestHelper:

    def __init__(self, adb):
        self.adb = adb

    def __convert_to_unicode_by_text(self, text):
        """
        Transfer input string to UTF-8 format
        """
        # Remove the unicode tag. example: transfer u'abc' to string abc
        return repr(text)[2:-1]

    def send_set_text_cmd(self, text):
        """
        Setting the input string to MyIME
        1. adb shell "am broadcast -a myIME.intent.action.pass.string -e input abc"
        2. adb shell input keyevent KEYCODE_UNKNOWN
        """
        self.adb.shell_cmd('\"am broadcast -a myIME.intent.action.pass.string -e input \\\"\"%s\"\\\"\"' %
                           TestHelper.__convert_to_unicode_by_text(self, text))
        self.adb.shell_cmd('input keyevent KEYCODE_UNKNOWN')


class ADB:

    def __init__(self, android_serial=None):
        self.buf = []

        adb_location = self.find_adb()
        self.buf.append('%s ' % adb_location)

        #self.buf.append('adb ')
        self.prefix_cmd = ''.join(self.buf)
        if android_serial is not None:
            self.buf.append('-s %s ' % android_serial)
            self.prefix_cmd = ''.join(self.buf)

    def cmd(self, cmd):
        self.buf = []
        self.buf.append(self.prefix_cmd)
        self.buf.append(cmd)
        cmd = ''.join(self.buf)
        return subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

    def shell_cmd(self, cmd):
        self.buf = []
        self.buf.append(self.prefix_cmd)
        self.buf.append('shell ')
        self.buf.append(cmd)
        cmd = ''.join(self.buf)
        return subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

    def find_adb(self):
        """

        :return: absloture path of adb or just adb
        """
        for adb_try in adb_locations:
            try:
                os.stat(adb_try)
                return adb_try
            except:
                pass

        raise ValueError('cannot find adb in %s' % str(adb_locations))


class Mobile():
    """
        DroydRunner Mobile keywords documentation

        Important Note:

            all keywords above have a mandatory implicit first argument:

            the mobile identifier ( the one you need with adb -s identifier to address a specific device )

            sample with keyword "Get Object | **keywords

                $obj= Get_Object $identifier **keywords

        The keywords marked "untested by ATAQ" are not tested for each release. They
        might work but are not guaranteed. Despite this you can try to use them. If
        you find a non working "untested" keyword, you can contact the support and
        we will try to make it tested and working for the next releases.

    """
    def __init__(self):
        self._set_serial(None)

    def _set_serial(self, android_serial):
        """
        Specify given *android_serial* device to perform test.

        You do not have to specify the device when there is only one device connects to the computer.

        When you need to use multiple devices, do not use this keyword to switch between devices in test execution.

        Using different library name when importing this library according to http://robotframework.googlecode.com/hg/doc/userguide/RobotFrameworkUserGuide.html?r=2.8.5.

        | Setting | Value  | Value     | Value   | 
        | Library | Mobile | WITH NAME | Mobile1 |
        | Library | Mobile | WITH NAME | Mobile2 |

        And set the serial to each library.

        | Test Case        | Action             | Argument           |
        | Multiple Devices | Mobile1.Set Serial | device_1's serial  |
        |                  | Mobile2.Set Serial | device_2's serial  |

        """
        self.adb = ADB(android_serial)
        self.device = Device(android_serial)
        self.test_helper = TestHelper(self.adb)

    def get_device_info(self):
        """
        Retrieve the device info.

        The keyword will return a dictionary.

        You can log the information by using the log dictionary keyword in build in Collections library(http://robotframework.googlecode.com/hg/doc/libraries/Collections.html?r=2.8.4).

        Example:
        | ${device_info} | Get Device Info |
        | Log Dictionary | ${device_info}  |

        =>

        Dictionary size is 9 and it contains following items:\n
        currentPackageName: com.android.keyguard\n
        displayHeight: 1776\n
        displayRotation: 0\n
        displaySizeDpX: 360\n
        displaySizeDpY: 640\n
        displayWidth: 1080\n
        naturalOrientation: True\n
        productName: hammerhead\n
        sdkInt: 19\n

        Or get specific information of the device by giving the key.

        | ${device_info}  | Get Device Info |   |                |
        | ${product_name} | Get From Dictionary | ${device_info} | productName |

        =>

        ${product_name} = hammerhead

        """
        return self.device.info

# Key Event Actions of the device
    """
    Turn on/off screen
    """

    def turn_on_screen(self):
        """
        Turn on the screen.
        """
        self.device.screen.on()

    def turn_off_screen(self):
        """
        Turn off the screen.
        """
        self.device.screen.off()

    """
    Press hard/soft key
    """

    # def press_key(self, *keys):
    #     """
    #     Press *key* keycode.
    #
    #     You can find all keycode in http://developer.android.com/reference/android/view/KeyEvent.html
    #
    #     """
    #     #not tested
    #     self.device.press(*keys)


    def press(self, key):
        """

        press a key

        key can be in [ home,back,right,left,up,down,center,menu,search,enter,delete,volume_up,volume_down,camera,power]
        or a keycode

        You can find all keycode in http://developer.android.com/reference/android/view/KeyEvent.html

        :param name: str name in home,back,left,right,...
        :return:
        """
        return self.device.press(key)

    def press_home(self):
        """
        Press home key.
        """
        self.device.press.home()

    # def press_back(self):
    #     """
    #     Press back key.
    #     """
    #     self.device.press.back()
    #
    # def press_left(self):
    #     """
    #     Press left key.
    #     """
    #     self.device.pres.left()
    #
    # def press_right(self):
    #     """
    #     Press right key.
    #     """
    #     self.device.press.right()
    #
    # def press_up(self):
    #     """
    #     Press up key.
    #     """
    #     self.device.press.up()
    #
    # def press_down(self):
    #     """
    #     Press down key.
    #     """
    #     self.device.press.down()
    #
    # def press_center(self):
    #     """
    #     Press center key.
    #     """
    #     self.device.press.center()
    #
    # def press_menu(self):
    #     """
    #     Press menu key.
    #     """
    #     self.device.press.menu()
    #
    # def press_search(self):
    #     """
    #     Press search key.
    #     """
    #     self.device.press.search()
    #
    # def press_enter(self):
    #     """
    #     Press enter key.
    #     """
    #     self.device.press.enter()
    #
    # def press_delete(self):
    #     """
    #     Press delete key.
    #     """
    #     self.device.press.delete()
    #
    # def press_recent(self):
    #     """
    #     Press recent key.
    #     """
    #     self.device.press.recent()
    #
    # def press_volume_up(self):
    #     """
    #     Press volume up key.
    #     """
    #     self.device.press.volume_up()
    #
    # def press_volume_down(self):
    #     """
    #     Press volume down key.
    #     """
    #     self.device.press.volume_down()
    #
    # def press_camera(self):
    #     """
    #     Press camera key.
    #     """
    #     self.device.press.camera()
    #
    # def press_power(self):
    #     """
    #     Press power key.
    #     """
    #     self.device.press.power()

# Gesture interaction of the device

    def click_at_coordinates(self, x, y):
        """
        Click at (x,y) coordinates.
        """
        self.device.click(int(x), int(y))

    def swipe_by_coordinates(self, sx, sy, ex, ey, steps=10):
        """
        Swipe from (sx, sy) to (ex, ey) with *steps* .

        Example:
        | Swipe By Coordinates | 540 | 1340 | 940 | 1340 |     | # Swipe from (540, 1340) to (940, 100) with default steps 10 |
        | Swipe By Coordinates | 540 | 1340 | 940 | 1340 | 100 | # Swipe from (540, 1340) to (940, 100) with steps 100        |
        """
        self.device.swipe(sx, sy, ex, ey, steps)

# Swipe from the center of the ui object to its edge

    # def swipe_left(self, steps=10, **selectors):
    #     """
    #     Swipe the UI object with *selectors* from center to left.
    #
    #     Example:
    #
    #     | Swipe Left | description=Home screen 3 |                           | # swipe the UI object left              |
    #     | Swipe Left | 5                         | description=Home screen 3 | # swipe the UI object left with steps=5 |
    #
    #     See `introduction` for details about Identified UI object.
    #     """
    #     self.device(**selectors).swipe.left(steps=steps)
    #
    # def swipe_right(self, steps=10,  **selectors):
    #     """
    #     Swipe the UI object with *selectors* from center to right
    #
    #     See `Swipe Left` for more details.
    #     """
    #     self.device(**selectors).swipe.right(steps=steps)
    #
    # def swipe_top(self, steps=10, **selectors):
    #     """
    #     Swipe the UI object with *selectors* from center to top
    #
    #     See `Swipe Left` for more details.
    #     """
    #     self.device(**selectors).swipe.up(steps=steps)
    #
    # def swipe_bottom(self, steps=10, **selectors):
    #     """
    #     Swipe the UI object with *selectors* from center to bottom
    #
    #     See `Swipe Left` for more details.
    #     """
    #     self.device(**selectors).swipe.down(steps=steps)
    #
    # def object_swipe_left(self, obj, steps=10):
    #     """
    #     Swipe the *obj* from center to left
    #
    #     Example:
    #
    #     | ${object}         | Get Object | description=Home screen 3 | # Get the UI object                     |
    #     | Object Swipe Left | ${object}  |                           | # Swipe the UI object left              |
    #     | Object Swipe Left | ${object}  | 5                         | # Swipe the UI object left with steps=5 |
    #     | Object Swipe Left | ${object}  | steps=5                   | # Swipe the UI object left with steps=5 |
    #
    #     See `introduction` for details about identified UI object.
    #     """
    #     obj = self.ui_object(obj)
    #     obj.swipe.left(steps=steps)
    #
    # def object_swipe_right(self, obj, steps=10):
    #     """
    #     Swipe the *obj* from center to right
    #
    #     See `Object Swipe Left` for more details.
    #     """
    #     obj = self.ui_object(obj)
    #     obj.swipe.right(steps=steps)
    #
    # def object_swipe_top(self, obj, steps=10):
    #     """
    #     Swipe the *obj* from center to top
    #
    #     See `Object Swipe Left` for more details.
    #     """
    #     obj = self.ui_object(obj)
    #     obj.swipe.up(steps=steps)
    #
    # def object_swipe_bottom(self, obj, steps=10):
    #     """
    #     Swipe the *obj* from center to bottom
    #
    #     See `Object Swipe Left` for more details.
    #     """
    #     obj = self.ui_object(obj)
    #     obj.swipe.down(steps=steps)

    def drag_by_coordinates(self, sx, sy, ex, ey, steps=10):
        """
        ** Untested by ATAQ **

        Drag from (sx, sy) to (ex, ey) with steps

        See `Swipe By Coordinates` also.
        """
        self.device.drag(sx, sy, ex, ey, steps)

    # Wait until the specific ui object appears or gone

    # wait until the ui object appears
    def wait_for_exists(self, timeout=0, **selectors):
        """
        ** Untested by ATAQ **

        Wait for the object which has *selectors* within the given timeout.

        Return true if the object *appear* in the given timeout. Else return false.
        """
        return self.device(**selectors).wait.exists(timeout=timeout)

    # wait until the ui object gone
    def wait_until_gone(self, timeout=0, **selectors):
        """
        ** Untested by ATAQ **

        Wait for the object which has *selectors* within the given timeout.

        Return true if the object *disappear* in the given timeout. Else return false.
        """
        return self.device(**selectors).wait.gone(timeout=timeout)

    def wait_for_object_exists(self, obj, timeout=0):
        """
        Wait for the object: obj within the given timeout.

        Return true if the object *appear* in the given timeout. Else return false.
        """
        obj = self.ui_object(obj)
        return obj.wait.exists(timeout=timeout)

    # wait until the ui object gone
    def wait_until_object_gone(self, obj, timeout=0):
        """
        Wait for the object: obj within the given timeout.

        Return true if the object *disappear* in the given timeout. Else return false.
        """
        obj = self.ui_object(obj)
        return obj.wait.gone(timeout=timeout)

    # Perform fling on the specific ui object(scrollable)
    def fling_forward_horizontally(self, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling forward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """
        return self.device(**selectors).fling.horiz.forward()

    def fling_backward_horizontally(self, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling backward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """
        return self.device(**selectors).fling.horiz.backward()

    def fling_forward_vertically(self, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling forward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """
        return self.device(**selectors).fling.vert.forward()

    def fling_backward_vertically(self, **selectors):
        """
        ** Untested by ATAQ **

        Perform fling backward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """
        return self.device(**selectors).fling.vert.backward()

    # Perform scroll on the specific ui object(scrollable)

    # horizontal
    def scroll_to_beginning_horizontally(self, steps=10, **selectors):
        """
        Scroll the object which has *selectors* attributes to *beginning* horizontally.

        See `Scroll Forward Vertically` for more details.
        """
        return self.device(**selectors).scroll.horiz.toBeginning(steps=steps)

    def scroll_to_end_horizontally(self, steps=10,  **selectors):
        """
        Scroll the object which has *selectors* attributes to *end* horizontally.

        See `Scroll Forward Vertically` for more details.
        """
        return self.device(**selectors).scroll.horiz.toEnd(steps=steps)

    def scroll_forward_horizontally(self, steps=10, **selectors):
        """
        Perform scroll forward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """
        return self.device(**selectors).scroll.horiz.forward(steps=steps)

    def scroll_backward_horizontally(self, steps=10, **selectors):
        """
        Perform scroll backward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """
        return self.device(**selectors).scroll.horiz.backward(steps=steps)

    def scroll_to_horizontally(self, obj, **selectors):
        """
        Scroll(horizontally) on the object: obj to specific UI object which has *selectors* attributes appears.

        Return true if the UI object, else return false.

        See `Scroll To Vertically` for more details.
        """
        obj= self.ui_object(obj)
        return obj.scroll.horiz.to(**selectors)

    # vertical
    def scroll_to_beginning_vertically(self, steps=10, **selectors):
        """
        Scroll the object which has *selectors* attributes to *beginning* vertically.

        See `Scroll Forward Vertically` for more details.
        """
        return self.device(**selectors).scroll.vert.toBeginning(steps=steps)

    def scroll_to_end_vertically(self, steps=10, **selectors):
        """
        Scroll the object which has *selectors* attributes to *end* vertically.

        See `Scroll Forward Vertically` for more details.
        """
        return self.device(**selectors).scroll.vert.toEnd(steps=steps)

    def scroll_forward_vertically(self, steps=10,  **selectors):
        """
        Perform scroll forward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        Example:
        | ${can_be_scroll} | Scroll Forward Vertically | className=android.widget.ListView       |                                   | # Scroll forward the UI object with class name |
        | ${can_be_scroll} | Scroll Forward Vertically | 100                                     | className=android.widget.ListView | # Scroll with steps |
        """
        return self.device(**selectors).scroll.vert.forward(steps=steps)

    def scroll_backward_vertically(self, steps=10, **selectors):
        """
        Perform scroll backward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """
        return self.device(**selectors).scroll.vert.backward(steps=steps)

    def scroll_to_vertically(self, obj, **selectors):
        """
        Scroll(vertically) on the object: obj to specific UI object which has *selectors* attributes appears.

        Return true if the UI object, else return false.

        Example:

        | ${list}        | Get Object           | className=android.widget.ListView |              | # Get the list object     |
        | ${is_web_view} | Scroll To Vertically | ${list}                           | text=WebView | # Scroll to text:WebView. |
        """
        obj=self.ui_object(obj)
        return obj.scroll.vert.to(**selectors)


# Screen Actions of the device

    def get_screen_orientation(self):
        """
        ** Untested by ATAQ **

        Get the screen orientation.

        Possible result: natural, left, right, upsidedown

        See for more details: https://github.com/xiaocong/uiautomator#screen-actions-of-the-device
        """
        return self.device.orientation

    def set_screen_orientation(self, orientation):
        """
        ** Untested by ATAQ **

        Set the screen orientation.

        Input *orientation* : natural or n, left or l, right or r, upsidedown (support android version above 4.3)

        The keyword will unfreeze the screen rotation first.

        See for more details: https://github.com/xiaocong/uiautomator#screen-actions-of-the-device

        Example:

        | Set Screen Orientation | n       | # Set orientation to natural |
        | Set Screen Orientation | natural | # Do the same thing          |
        """
        self.device.orientation = orientation

    def freeze_screen_rotation(self):
        """
        ** Untested by ATAQ **

        Freeze the screen auto rotation
        """
        self.device.freeze_rotation()

    def unfreeze_screen_rotation(self):
        """
        ** Untested by ATAQ **

        Un-Freeze the screen auto rotation
        """
        self.device.freeze_rotation(False)

    # def screenshot(self, scale=None, quality=None):
    #     """
    #     Take a screenshot of device and log in the report with timestamp, scale for screenshot size and quality for screenshot quality
    #     default scale=1.0 quality=100
    #     """
    #     output_dir = BuiltIn().get_variable_value('${OUTPUTDIR}')
    #     ts = time.time()
    #     st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
    #     screenshot_path = '%s%s%s.png' % (output_dir, os.sep, st)
    #     self.device.screenshot(screenshot_path, scale, quality)
    #     logger.info('\n<a href="%s">%s</a><br><img src="%s">' % (screenshot_path, st, screenshot_path), html=True)

    def _screenshot(self, scale=1.0, quality=100):
        """

            low level screen shot: return a base64 image string

        :param scale:
        :param quality:
        :return:
        """
        try:
            os.unlink('image.png')
        except OSError:
            pass
        self.device.screenshot("image.png", scale=scale, quality=quality)
        with open("image.png", "rb") as fh:
            data = fh.read()
            encoded = data.encode("base64")
            os.unlink("image.png")
            # print("flash:\n")
            # print encoded
            # print("----\n")
            return encoded

    def screenshot(self, name=None):
        """
            build a png file from a screenshot

        :param name: str , basename for the generated png file

        :return: str , the full name of the file generated
        """
        pass
        # dummy function , just for documentation

    def _dump(self):
        """
            low level dump hierarchy: return a xml unicode

        :return:
        """
        content = self.device.dump()
        # print "dump:\n"
        # print content
        # print "---\n"
        return unicode(content)

    def dump(self, name=None):
        """

            dump an xml hierarchy for the current screen

        :param name: str ,basename for the generated uix file

        :return: str , the full name of the file generated
        """
        pass
        # dummy function , just for documentation

    def snapshot(self, name=None):
        """
            perform a screenshot (name.png) and a dump (name.uix)

        :param name: str , basename for the generated  files

        :return: str , the full name of the file generated , without extension
        """
        pass
        # dummy function , just for documentation


# Watcher
    def __unicode_to_dict(self, a_unicode):
        a_dict = dict()
        dict_item_count = a_unicode.count('=')
        for count in range(dict_item_count):
            equal_sign_position = a_unicode.find('=')
            comma_position = a_unicode.find(',')
            a_key = a_unicode[0:equal_sign_position]
            if comma_position == -1:
                a_value = a_unicode[equal_sign_position + 1:]
            else:
                a_value = a_unicode[equal_sign_position + 1:comma_position]
                a_unicode = a_unicode[comma_position + 1:]
            a_dict[a_key] = a_value
        return a_dict

    def register_click_watcher(self, watcher_name, selectors, *condition_list):
        """
        ** Untested by ATAQ **

        The watcher click on the object which has the *selectors* when conditions match.
        """
        watcher = self.device.watcher(watcher_name)
        for condition in condition_list:
            watcher.when(**self.__unicode_to_dict(condition))
        watcher.click(**self.__unicode_to_dict(selectors))
        self.device.watchers.run()

    def register_press_watcher(self, watcher_name, press_keys, *condition_list):
        """
        ** Untested by ATAQ **

        The watcher perform *press_keys* action sequentially when conditions match.
        """
        def unicode_to_list(a_unicode):
            a_list = list()
            comma_count = a_unicode.count(',')
            for count in range(comma_count + 1):
                comma_position = a_unicode.find(',')
                if comma_position == -1:
                    a_list.append(str(a_unicode))
                else:
                    a_list.append(a_unicode[0:comma_position])
                    a_unicode = a_unicode[comma_position + 1:]
            return a_list

        watcher = self.device.watcher(watcher_name)
        for condition in condition_list:
            watcher.when(**self.__unicode_to_dict(condition))
        watcher.press(*unicode_to_list(press_keys))
        self.device.watchers.run()

    def remove_watchers(self, watcher_name=None):
        """
        ** Untested by ATAQ **

        Remove watcher with *watcher_name* or remove all watchers.
        """
        if watcher_name == None:
            self.device.watchers.remove()
        else:
            self.device.watchers.remove(watcher_name)

    def list_all_watchers(self):
        """
        ** Untested by ATAQ **

        Return the watcher list.
        """
        return self.device.watchers

# Selector

    def get_object(self, **selectors):
        """
        Get the UI object with selectors *selectors*

        See `introduction` for details about identified UI object.

        Example:
        | ${main_layer} | Get Object | className=android.widget.FrameLayout | index=0 | # Get main layer which class name is FrameLayout |
        """
        # return self.device(*args, **selectors)
        obj = self.device(**selectors)
        if obj.exists:
            data=self.flatten_ui_objet(obj)
            return data
        else:
            return None

        # # serialize AutomatorDeviceObject to a dict
        # selector= obj.selector
        # serialized_object = dict((k, selector[k]) for k in selector
        #               if k not in ['mask', 'childOrSibling', 'childOrSiblingSelector'])
        # #serialized_object= obj.selector.clone()
        # return serialized_object

    def get_child(self, obj, **selectors):
        """
        ** Untested by ATAQ **

        Get the child or grandchild UI object from the *object* with *selectors*
        Example:
        | ${root_layout}   | Get Object | className=android.widget.FrameLayout |
        | ${child_layout}  | Get Child  | ${root_layout}                       | className=LinearLayout |
        """
        if obj:
            obj = self.ui_object(obj)
            if obj.exists:
                child= obj.child(**selectors)
                if child.exists:
                    return child.info
        return {}

    def get_sibling(self, obj, **selectors):
        """
        ** Untested by ATAQ **

        Get the sibling or child of sibling UI object from the *object* with *selectors*
        Example:
        | ${root_layout}     | Get Object   | className=android.widget.FrameLayout |
        | ${sibling_layout}  | Get Sibling  | ${root_layout}                       | className=LinearLayout |
        """
        if obj:
            obj = self.ui_object(obj)
            if obj.exists:
                child= obj.sibling(**selectors)
                if child.exists:
                    return child.info
        return {}

    def get_count(self,  **selectors):
        """
        ** Untested by ATAQ **

        Return the count of UI object with *selectors*

        Example:

        | ${count}              | Get Count           | text=Accessibility    | # Get the count of UI object text=Accessibility |
        | ${accessibility_text} | Get Object          | text=Accessibility    | # These two keywords combination                |
        | ${count}              | Get Count Of Object | ${accessibility_text} | # do the same thing.                            |

        """
        obj = self.device(**selectors)
        if obj.exists:
            return len(obj)
        return 0

    def get_count_of_object(self, obj):
        """
        ** Untested by ATAQ **

        Return the count of given UI object

        See `Get Count` for more details.
        """
        if obj:
            obj = self.ui_object(obj)
            if obj.exists:
                return len(obj)
        return 0

    def get_info_of_object(self, obj, selector=None):
        """
        return info dictionary of the *obj*
        The info example:
        {
         u'contentDescription': u'',
         u'checked': False,
         u'scrollable': True,
         u'text': u'',
         u'packageName': u'com.android.launcher',
         u'selected': False,
         u'enabled': True,
         u'bounds': 
                   {
                    u'top': 231,
                    u'left': 0,
                    u'right': 1080,
                    u'bottom': 1776
                   },
         u'className': u'android.view.View',
         u'focusable': False,
         u'focused': False,
         u'clickable': False,
         u'checkable': False,
         u'chileCount': 1,
         u'longClickable': False,
         u'visibleBounds':
                          {
                           u'top': 231,
                           u'left': 0,
                           u'right': 1080,
                           u'bottom': 1776
                          }
        }
        """
        if obj:
            obj = self.ui_object(obj)
            if obj.exists:
                if selector:
                    return obj.info.get(selector)
                else:
                    return obj.info
        return {}

    def click(self, **selectors):
        """
        Click on the UI object with *selectors*

        | Click | text=Accessibility | className=android.widget.Button | # Click the object with class name and text |
        """
        self.device(**selectors).click()

    def click_on_object(self, object):
        """
        ** Untested by ATAQ **

        Click on the UI object which is gained by `Get Object`.

        Example:
        | ${button_ok}    | text=OK      | className=android.widget.Button |
        | Click on Object | ${button_ok} |
        """
        return self.call(obj=object, method='click')
        # return object.click()

    def long_click(self, **selectors):
        """
        Long click on the UI object with *selectors*

        See `Click` for more details.
        """
        self.device(**selectors).long_click()

    def ui_object(self, obj):
        """
        ** Untested by ATAQ **


            return a ui object from a dictionary of selector properties

        :param obj: instance of AutomatorDeviceObject , or dict ( selector properties )
        :return: return a local device ui object
        """
        # if obj is not a Uidevice instance, build a UiDevice with obj as a
        # selector dictionary
        if not isinstance(obj, AutomatorDeviceObject):
            # adapt to http call
            assert isinstance(obj, dict)
            obj = self.device(**obj)
        return obj

    def flatten_ui_objet(self, obj):
        """
        ** Untested by ATAQ **

            serialize a ui object to a dict of selector properties

        :param obj: instance of  AutomatorDeviceObject
        :return: dict : dict of selector properties
        """
        selector = obj.selector
        serialized_object = dict((k, selector[k]) for k in selector
                                 if k not in ['mask', 'childOrSibling', 'childOrSiblingSelector'])
        return serialized_object

    def call(self, obj, method, **selectors):
        """
        ** Untested by ATAQ **

        This keyword can use object method from original python uiautomator

        See more details from https://github.com/xiaocong/uiautomator

        Example:

        | ${accessibility_text} | Get Object            | text=Accessibility | # Get the UI object                        |
        | Call                  | ${accessibility_text} | click              | # Call the method of the UI object 'click' |
        """
        obj = self.ui_object(obj)
        func = getattr(obj, method)
        return func(**selectors)

    def set_text(self, input_text, **selectors):
        """
        ** Untested by ATAQ **

        Set *input_text* to the UI object with *selectors* 
        """
        self.device(**selectors).set_text(input_text)

    def set_object_text(self, input_text, obj):
        """
        ** Untested by ATAQ **

        Set *input_text* the *object* which could be selected by *Get Object* or *Get Child*
        """
        obj = self.ui_object(obj)
        obj.set_text(input_text)



# Other feature

    def clear_text(self, **selectors):
        """
        ** Untested by ATAQ **

        Clear text of the UI object  with *selectors*
        """
        while True:
            target = self.device(**selectors)
            text = target.info['text']
            target.clear_text()
            remain_text = target.info['text']
            if text == '' or remain_text == text:
                break

    def open_notification(self):
        """
        Open notification

        Built in support for Android 4.3 (API level 18)

        Using swipe action as a workaround for API level lower than 18

        """
        sdk_version = self.device.info['sdkInt']
        if sdk_version < 18:
            height = self.device.info['displayHeight']
            self.device.swipe(1, 1, 1, height - 1, 1)
        else:
            self.device.open.notification()

    def open_quick_settings(self):
        """
        Open quick settings

        Work for Android 4.3 above (API level 18)

        """
        self.device.open.quick_settings()

    def _sleep(self, time):
        """
        Sleep(no action) for *time* (in millisecond)
        """
        target = 'wait for %s' % str(time)
        self.device(text=target).wait.exists(timeout=time)

    def install(self, apk_path):
        """
        ** Untested by ATAQ **

        Install apk to the device.

        Example:

        | Install | ${CURDIR}${/}com.hmh.api_4.0.apk | # Given the absolute path to the apk file |
        """
        self.adb.cmd('install "%s"' % apk_path)

    def uninstall(self, package_name):
        """
        ** Untested by ATAQ **

        Uninstall the APP with *package_name*
        """
        self.adb.cmd('uninstall %s' % package_name)

    def execute_adb_command(self, cmd):
        """
        Execute adb *cmd*
        """
        return self.adb.cmd(cmd)

    def execute_adb_shell_command(self, cmd):
        """
        Execute adb shell *cmd*
        """
        return self.adb.shell_cmd(cmd)

    def type(self, input_text):
        """
        ** Untested by ATAQ **

        [IME]

        Type *text* at current focused UI object
        """
        self.test_helper.send_set_text_cmd(input_text)

    def start_test_agent(self):
        """
        ** Untested by ATAQ **

        [Test Agent]

        Start Test Agent Service
        """
        cmd = 'am start edu.ntut.csie.sslab1321.testagent/edu.ntut.csie.sslab1321.testagent.DummyActivity'
        self.adb.shell_cmd(cmd)

    def stop_test_agent(self):
        """
        ** Untested by ATAQ **

        [Test Agent]

        Stop Test Agent Service
        """
        cmd = 'am broadcast -a testagent -e action STOP_TESTAGENT'
        self.adb.shell_cmd(cmd)

    # def connect_to_wifi(self, ssid, password=None):
    #     """
    #     ** Untested by ATAQ **
    #
    #     [Test Agent]
    #
    #     Connect to *ssid* with *password*
    #     """
    #     cmd = 'am broadcast -a testagent -e action CONNECT_TO_WIFI -e ssid %s -e password %s' % (
    #         ssid, password)
    #     self.adb.shell_cmd(cmd)
    #
    # def clear_connected_wifi(self):
    #     """
    #     ** Untested by ATAQ **
    #
    #     [Test Agent]
    #
    #     Clear all existed Wi-Fi connection
    #     """
    #     cmd = 'am broadcast -a testagent -e action CLEAR_CONNECTED_WIFIS'
    #     self.adb.shell_cmd(cmd)


    # old work
    ## adds
    def _get_xml_hierarchy(self,source=None):
        """

        :param source: dump file or None
        :return: an instance of Elementree
        """
        if source:
            # get hierarchy from source file
            with open(source,'r') as fh:
                content= source.read()
                content=content.encode(encoding=("utf-8"))

        else:
            # get live hierarchy
            dmp=self.device.dump(compressed=False)
            content= dmp.encode(encoding='utf-8')
        # make element tree
        xml= ET.fromstring(content)
        return xml


    def _lt_next_index(self,element,index):
        """
            return the child with the given index in a aml node

        :param element: xml element
        :param index:
        :return:
        """
        for child in element:
            if child.attrib['index'] == index :
                # we found an element , return it
                return child
        #found nothing
        return None


    def _lt_get_index_path_node(self,element,index):
        """

        :param element: etree element
        :param index: string the path like "0/1/0"
        :return:
        """
        first=True
        for idx in index.split('/'):
            child= self._lt_next_index(element,idx)
            if child is not None:
                # found something
                first=False
                element=child
            else:
                # found nothing
                if first:
                    raise ValueError("more than one node with same index found: use different selectors")
                else:
                    raise ValueError("no such node, Maximum index value at current node is %d" % (len(element)-1))
        return element


    def _lt_element_and_coordinates_by_index_path(self, hierarchy,index_path):

        """
            look for an element given the index path

            return the selected xml element , x and y coordinates of an index path


        :param hierarchy: etree Element: root of a uiautomator xml dump
        :param index_path:
        :return:
        """
        element= self._lt_get_index_path_node(hierarchy,index_path)

        # getting bounds' coordinates and click
        if not element.attrib.has_key("bounds"):
            raise AttributeError("Selected element has no bounds determined by coordinates")
        bounds = re.findall("\[([0-9]+)\,([0-9]+)\]", element.attrib["bounds"])
        avg_x = (int(bounds[0][0]) + int(bounds[1][0]))/2
        avg_y = (int(bounds[0][1]) + int(bounds[1][1]))/2

        return element , avg_x , avg_y

    def _lt_action_by_index(self, index , function='click', **selectors):
        """

        :param index:
        :param selectors:
        :return:
        """
        xml= self._get_xml_hierarchy()

        element,x,y = self._lt_element_and_coordinates_by_index_path(xml,index)

        # check element with given selectors
        found=True
        for selector_name,selector_value in selectors.iteritems():
            xml_selector= self._selector_ui2xml(selector_name)

            if xml_selector in element.attrib:
                if not element.attrib[xml_selector] == selector_value:
                    found= False
                    break
            else:
                raise AttributeError("No such attribute %s in selected element" % selector_name)

        if found:
            if hasattr(self.device, function):
                try:
                    # call the device action (click, long click)
                    return getattr(self.device, function) (x, y)
                except Exception as e:
                    raise AttributeError("2 int() objects must be accepted by '%s' method"%function +
                                         "[original error:" + e.message + "]")
            else:
                raise AttributeError("'%s' method does not exist"%function)
        else:
            raise AttributeError("Element not found")


    def _selector_ui2xml(self,ui_selector_name):
        """
            convert a ui selector name to xml dump attribute name
                eg className -> class , resourceId -> resource-id

        :param ui_selector_name:
        :return:
        """
        convert= {
            'className': 'class', 'resourceId': 'resource-id'
        }
        try:
            return convert[ui_selector_name]
        except KeyError:
            # assume same name
            return ui_selector_name

    # def lt_get_info_by_index (self, index, selector_name):
    #     """
    #     Returns the data contained by the attribute specified by 'selector'.
    #     """
    #     #dmp=self.device.dump(compressed=False)
    #     #buf= dmp.encode(encoding='utf-8')
    #     #xml= ET.fromstring(buf)
    #     xml= self._get_xml_hierarchy()
    #
    #     element,x,y= self._lt_element_and_coordinates_by_index_path(xml,index)
    #     try:
    #         xml_selector=self._selector_ui2xml(selector_name)
    #         return element.attrib[xml_selector]
    #     except KeyError:
    #         raise AttributeError("No such attribute %s for the selected node." % selector_name)
    #
    #
    # def lt_click_by_index (self, index, **selectors):
    #     """
    #     Makes a click on the given element specified by its index path
    #     """
    #     return self._lt_action_by_index(index=index, function='click', **selectors)
    #
    #
    # def lt_long_click_by_index (self, index, **selectors):
    #     """
    #     Makes a long click on the given element specified by its index path
    #     """
    #     return self._lt_action_by_index(index=index,function='long_click',**selectors)


# Indexpath integration (Rado)

    def get_info_by_index(self, index, selector, dump_file=None):
        """
        Return the data contained by the attribute specified by 'selector'.
        :param selector: the selector type of the element you want to read from.
        selectors can be any of the element's attributes.
        :param index: Index path. Separated by slashes.
        :param dump_file: XML file on which to operate, instead of a XML screen dump file.

        :return unicode: unicode() object containing the selector value.

        :raise AttributeError: if the specified selector is not found in the element
        specified by the index path.
        :raise ValueError: If no node found, or if more than one node with
        the same index path has been found (i.e. the XML dump is not well-formed),
        or if the selected attribute does not have the same value as expected.
        """
        xml_txt = u""
        if dump_file is None:
            xml_txt = self.device.dump(compressed=False)
        else:
            xml_file = file(dump_file, "r")
            xml_txt = xml_file.read()
            xml_file.close()

        ipathdata = self._get_index_path_node(ET.fromstring(xml_txt), index)
        selector = self._selector_ui2xml(selector)
        if selector in ipathdata.attrib:
            return unicode(ipathdata.attrib[selector])
        else:
            raise AttributeError("No such attribute for the selected node.")


    def click_by_index(self, index, dump_file=None, **selectors):
        """
        Makes a click on the given element specified by its index path.
        The following arguments are required:

        :param index: Index path. Separated by slashes.
        :param <selector>: any of the elements attributes,
        including but not limited to: className, resourceId, bounds, text, &c.

        :raise AttributeError: view error messages for details.
        :raise ValueError: If no node found, or if more than one node with
        the same index path has been found (i.e. the XML dump is not well-formed),
        or if the selected attribute does not have the same value as expected.
        """
        self._action_by_index(index, 'click', dump_file, **selectors)


    def long_click_by_index(self, index, dump_file=None, **selectors):
        """
        Makes a click on the given element specified by its index path.
        :param index: Index path. Separated by slashes.
        :param <selector>: any of the elements attributes,
        including but not limited to: className, resourceId, bounds, text, &c.

        :raise AttributeError: view error messages for details.
        :raise ValueError: If no node found, or if more than one node with
        the same index path has been found (i.e. the XML dump is not well-formed),
        or if the selected attribute does not have the same value as expected.
        """
        self._action_by_index(index, 'long_click', dump_file, **selectors)


    def _action_by_index(self, index, function, dump_file=None, **selectors):
        """
        :type selectors: object
        """
        d = self.device

        xml_txt = d.dump(compressed=False)
        if dump_file is not None:
            xml_file = file(dump_file, "r")
            xml_txt = xml_file.read()
            xml_file.close()

        # selector (the first undeleted kwarg)
        if len(tuple(selectors)) == 0:
            raise IndexError("No selector type defined.")
        selector_name = self._selector_ui2xml(tuple(selectors)[0])
        selector_value = selectors[selector_name]

        ipath_data = self._get_index_path_node(ET.fromstring(xml_txt), index)

        if selector_name in ipath_data.attrib:
            if ipath_data.attrib[selector_name] == selector_value:
                # getting bounds' coordinates and click
                if "bounds" not in ipath_data.attrib:
                    raise AttributeError(
                        "Selected element has no bounds determined by coordinates")
                bounds = re.findall(
                    "\[([0-9]+)\,([0-9]+)\]", ipath_data.attrib["bounds"])
                avg_x = (int(bounds[0][0]) + int(bounds[1][0])) / 2
                avg_y = (int(bounds[0][1]) + int(bounds[1][1])) / 2
                if hasattr(d, function):
                    try:
                        getattr(d, function)(avg_x, avg_y)
                    except Exception as e:
                        err_msg = "2 int() objects must be accepted by '%s' method" % function
                        raise AttributeError(
                            err_msg + "[original error:" + e.message + "]")
                else:
                    raise AttributeError(
                        "'%s' method does not exist" % function)
            else:
                raise AttributeError(
                    "No such attribute value in selected element")
        else:
            raise AttributeError("No such attribute in selected element")


    def _get_index_path_node(self, parsed_xml, xipath):
        """uses "index" attributes instead of positions"""
        current_depth = 0
        current_node = parsed_xml
        for index in xipath.split('/'):
            new_node = None
            for child in current_node:
                # print child
                if child.attrib['index'] == index:
                    if new_node is not None:
                        raise ValueError("more than one node with the same index found. "
                                         "Please use different selector.")
                    else:
                        new_node = child
            if new_node is None:
                raise ValueError("no such node. Maximum index value at current node is %d (current indexpath node is #%d)"
                                 % (len(current_node) - 1, current_depth))
            current_node = new_node
            current_depth += 1

        return current_node

    ### implements keywords for upgrade to uiautomator 0.2.4
    def native_operation( self, operation='info', modifiers='' ,**selectors):
        """


        :param selector: selector like d(className="android.widget.ListView
        :param modifier: literal like child_by_text("Wi-Fi",className="android.widget.LinearLayout")
        :param operation: click , long_click
        :return: return code of operation

        possible modifiers ( link modifiers with . )

        child(),child_by_text( text,className="" ),child_by_description(),child_by_instance()
        sibling()
        left(),right(className="android.widget.Switch")
        up(),down()


        operation must one of exists,info,count,click,click.bottomright,click.topleft,
            long_click,long_click.bottomright,long_click.topleft,clear_text
        """
        assert operation in [ 'exists' , 'info', 'count',
                              'click','click.bottomright', 'click.topleft',
                              'long_click','long_click.bottomright', 'long_click.topleft',
                              'clear_text',
                              ]
        d=self.device
        uio = d(**selectors)
        if modifiers:
            cmd = 'uio.%s.%s' % ( modifiers, operation )
        else:
            cmd = 'uio.%s' % ( operation )
        if operation not in ['exists', 'info','count']:
            cmd += '()'

        r= eval(cmd)
        return r

    def native_drag_to(self, target='' , steps=10, **selectors):
        """
        Drag object reached with  **selectors to target

        target: can be   x=1 ,y=2 or text="Clock"

        """
        uio= self.device(**selectors)
        if uio:
            cmd= "uio.drag.to( steps=%s, %s )" %  (str(steps),target)
            return eval(cmd)
        else:
            return None

    def native_fling(self,modifiers='',max_swipes=1000,**selectors):
        """
        ** Untested by ATAQ **

        Perform fling on the specific scrollable ui object reachable via **selectors

        Possible modifiers:

            forward (default) , verticaly (default)
            backward          , horizentally
            toBeginning       ,  toEnd

            horiz.toBeginning , horiz.toEnd , horiz.backward , horiz.forward
            vert.toBeginning , vert.toEnd , vert.backward , vert.forward

        """
        assert modifiers in ['','forward',  'backward',
                                'verticaly', 'horizentally',
                                'toBeginning', 'toEnd' ,
                                 'horiz.toBeginning' , 'horiz.toEnd' , 'horiz.backward' , 'horiz.forward',
                                 'vert.toBeginning' ,  'vert.toEnd' ,  'vert.backward' ,  'vert.forward',
                        ]
        uio= self.device(**selectors)
        if uio:
            if modifiers:
                cmd= "fling.%s(max_swipes=%s)" % str(max_swipes)
            else:
                cmd="fling(max_swipes=%s)" % str(max_swipes)
            return eval(cmd)
        else:
            return None

    def native_scroll(self,modifiers='',steps=100,max_swipes=1000,**selectors):
        """

        Perform scroll on the specific ui object(scrollable) reachable via **selectors

        Possible properties:

        horiz or vert
        forward or backward or toBeginning or toEnd, or to(text=..)


        :return:
        """
        if not modifiers in ['','forward',  'backward',
                                'verticaly', 'horizentally',
                                'toBeginning', 'toEnd' ,
                                 'horiz.toBeginning' , 'horiz.toEnd' , 'horiz.backward' , 'horiz.forward',
                                 'vert.toBeginning' ,  'vert.toEnd' ,  'vert.backward' ,  'vert.forward',
                                ] and not modifiers.startswith('to('):
            raise AssertionError("bad native_scroll modifiers: %s" % modifiers)

        uio= self.device(**selectors)
        if uio:
            if modifiers:
                if modifiers.startswith('to('):
                    text= modifiers.replace('to(','').replace(')','')
                    #  scroll to object
                    cmd= "scroll.to( %s, steps=%s, max_swipes=%s)" % (text,str(steps),str(max_swipes))
                else:
                    # scroll relative
                    cmd= "scroll.%s(steps=%s,max_swipes=%s)" % (modifiers,str(steps),str(max_swipes))
            else:
                cmd="scroll(steps=%s,max_swipes=%s)" % str(max_swipes)
            return eval(cmd)
        else:
            return None

    def native_gesture(self,gesture='',**selectors):
        """
        ** Untested by ATAQ **

        Two point gesture from one point to another

            d(text="Settings").gesture((sx1, sy1), (sx2, sy2)).to((ex1, ey1), (ex2, ey2))

            gesture=  gesture((sx1, sy1), (sx2, sy2)).to((ex1, ey1), (ex2, ey2))


        :return:
        """
        assert gesture.startswith('gesture(')
        uio = self.device(**selectors)
        if uio:
            cmd = 'uio.' % ( gesture )
            r= eval(cmd)
        else:
            r=None
        return r


    def swipe_left(self,steps=10,**selectors):
        """
            see swipe
        """
        return self.device(**selectors).swipe.left(steps=steps)

    def swipe_right(self,steps=10,**selectors):
        """
            see swipe
        """
        return self.device(**selectors).swipe.right(steps=steps)

    def swipe_up(self,steps=10,**selectors):
        """
            see swipe
        """
        return self.device(**selectors).swipe.up(steps=steps)

    def swipe_down(self,steps=10,**selectors):
        """
            see swipe
        """
        return self.device(**selectors).swipe.down(steps=steps)

    def pinch_in(self,percent=100,steps=10,**selectors):
        """
        ** Untested by ATAQ **

            Two point gesture on the specific ui object reachable via **selectors

            In, from edge to center
        """
        return self.device(**selectors).pinch.In(percent=percent,steps=steps)

    def pinch_out(self,percent=100,steps=10,**selectors):
        """
        ** Untested by ATAQ **

           Two point gesture on the specific ui object reachable via **selectors

            Out, from center to edge
        """
        return self.device(**selectors).pinch.Out(percent=percent,steps=steps)

