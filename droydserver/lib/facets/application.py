__author__ = 'cocoon'



from flask import Flask ,jsonify

from error_handler import InvalidUsage ,ApplicationError






# get a flask application
def get_application(name,with_error_handler=True):
    """

        get a flask application

    :return:
    """

    app= Flask(name)


    if  with_error_handler:
        # plug facets error Handlers to the app
        @app.errorhandler(InvalidUsage)
        def handle_invalid_usage(error):
            response = jsonify(error.to_dict())
            response.status_code = error.status_code
            return response

        @app.errorhandler(ApplicationError)
        def handle_invalid_usage(error):
            response = jsonify(error.to_dict())
            response.status_code = error.status_code
            return response


    return app