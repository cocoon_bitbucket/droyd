__author__ = 'cocoon'
"""
    a collection to handle devices configuration


    /devices
        GET: list device
        POST: create device

    /devices/<device_id>
        GET: get device parameters




"""


from flask import request
from flask import json
from droydserver.lib.facets.rest_collections import CollectionWithOperationApi

#from handle_error import InvalidUsage, ApplicationError


class HubDevices(CollectionWithOperationApi):
    """

        collection of device parameters

    """


    def post(self):
        """
            create an item

            POST /collection                        # create an item

        :return:
        """
        #try:
        db = self.config['db']


         # create a dummy session
        r =request

        data = request.json
        data = json.loads(r.data)

        device_id = data['device_id']

        # create session
        db.add_entity('devices',device_id,data)

        r = db.get('devices',device_id)

        response = dict( result =  r , text = "creation OK")


        #s = { 'session': session_id, 'Alice': 'Alice-123' , 'Bob': 'Bob-123' }
        return json.dumps(response)

        #except:
        #    raise ApplicationError("cant create item")


    def get(self,item=None):
        """
            get /collection :         return list of items
            get /collection/<item> :  return item content

        """
        db = self.config['db']

        if item is None:
            # return list of users
            #return NotImplementedError
            #raise InvalidUsage('Not implemented', status_code=400)
            raise NotImplementedError

        else:
            # return single item

            #try:
            r = db.get('devices',item)
            response = dict( result =  r , text = "read device config")

            #except:
            #    raise ApplicationError('error while getting device %s' % item )


            return json.dumps(response)