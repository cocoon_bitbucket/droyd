#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""droyd hub server

Usage:
  droydserve  [--host <host> --port <port>  --debug]
  droydserve (-h | --help)
  droydserve --version

Options:
  -h --help         Show this screen.
  --version         Show version.
  -H --host <host>  host [default: 0.0.0.0] .
  -p --port <port>  port [default: 5000] .
  -d --debug        debug mode

"""
import sys
from docopt import docopt

from droydserver.server.app import start

version = '0.0.1'


def main():
    args = docopt(__doc__, version=version)



    rs = start( host=args['--host'], port=int(args['--port']),debug=args['--debug'])


    return rs


if __name__ == '__main__':
    sys.exit(main())
